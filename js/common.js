$(document).ready(function(){

  $('.main-nav__mobile-button').click(function() {

    $('body, html').addClass('lock-screen');
    $('.main-nav__block').show();
    $('.main-nav__list').animate({left: 0}, 500);

    });

  $('.main-nav__block').click(function(e) {

    if($(e.target).hasClass('main-nav__block')) {
      $('.main-nav__list').animate({left: -220}, 500);
      setTimeout(
        function() {
          $('.main-nav__block').hide();
        }, 500);   
      $('body, html').removeClass('lock-screen');  
    }

  });
  	
});