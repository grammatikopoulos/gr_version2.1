<?php
define('__ROOT__', dirname(__FILE__));
require_once ('./includes/content.php');
require_once ('./includes/ViewRenderer.php');



$repo = new ContentPageRepository();
$pageUrl = $_SERVER['REQUEST_URI'];
if(strpos($pageUrl, '/test') === 0) {
    $pageUrl = substr($pageUrl,5);
}
$page = $repo->getContentPage($pageUrl);
//header('Content-Type: text/html; charset=utf-8');


switch ($pageUrl) {
    case '/regle-roulette.html':
        echo ViewRenderer::render('regle-roulette.php', $page);
        break;
    case '/strategie-roulette.html':
        echo ViewRenderer::render('strategies.php', $page);
        break;
    default :
        echo ViewRenderer::render('index.php', $page);
}



