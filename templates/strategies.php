
<!DOCTYPE html>
<html lang="fr">

<?php
    echo ViewRenderer::render('header.php', $data);
?>

    <nav class="main-nav">
      <div class="main-nav__container container">
        <button class="main-nav__mobile-button">
          <span></span>
          <span></span>
          <span></span>
        </button>
        <div class="main-nav__block">
          <ul class="main-nav__list">
            <li class="main-nav__item">
              <a class="main-nav__link" href="/">Accueil</a>
            </li>
            <li class="main-nav__item">
              <a class="main-nav__link" href="#">Top Casino</a>
            </li>
            <li class="main-nav__item">
              <a class="main-nav__link" href="#">Règles</a>
            </li>
            <li class="main-nav__item main-nav__active">
              <span class="main-nav__link">Stratégies</span>
            </li>
            <li class="main-nav__item">
              <a class="main-nav__link" href="bonus.html">Bonus</a>
            </li>
            <li class="main-nav__item">
              <a class="main-nav__link" href="guide.html">Guide du joueur</a>
            </li>
            <li class="main-nav__item">
              <a class="main-nav__link" href="free.html">Jeux gratuits</a>
            </li>
          </ul>
        </div>
      </div><!-- container -->
    </nav><!-- main-nav -->

    <div class="inner-page container">
      <h1><?php echo $data->main_title; ?></h1>
      <div class="text-block strategies__text">
        <p><?php echo $data->main_text; ?></p>
      </div>
      <ul class="strategies-list">
        <li class="strategies__item">
          <div class="strategies__icon">
            <img src="img/strategies_icon_1.svg" alt="">
          </div>
          <div class="strategies__content">
            <p class="strategies__description">
              <span class="strategies__title">La Martingale Prolongée</span>
               - Connue sous divers noms, la Martingale Prolongée est une montante qui a pour objectif de réduire les conséquences néfastes des martingales plus traditionnelles. En effet, ces dernières causent parfois de lourdes pertes au joueur....
            </p>
            <a href="#" class="button button--secondary strategies__button">Lire la suite</a>
          </div>
        </li>
        <li class="strategies__item">
          <div class="strategies__icon">
            <img src="img/strategies_icon_2.svg" alt="">
          </div>
          <div class="strategies__content">
            <p class="strategies__description">
              <span class="strategies__title">La Martingale à Dents de Scie</span>
               - La Martingale à Dents de Scie fait partie de la famille des martingales montantes à la roulette. Ce type de martingale présente un avantage indéniable : en effet, elle permet de limiter assez fortement la casse en cas de longue série perdante....
            </p>
            <a href="#" class="button button--secondary strategies__button">Lire la suite</a>
          </div>
        </li>
        <li class="strategies__item">
          <div class="strategies__icon">
            <img src="img/strategies_icon_3.svg" alt="">
          </div>
          <div class="strategies__content">
            <p class="strategies__description">
              <span class="strategies__title">La Martingale Cyclique</span>
               - La Martingale Cyclique est une méthode adaptée aux jeux de roulette qui parle d’elle-même. Cette dernière permet en effet d’effacer les pertes du joueur par cycles. On peut l’utiliser aux jeux de casino online qui sollicitent l’utilisation d’une...
            </p>
            <a href="#" class="button button--secondary strategies__button">Lire la suite</a>
          </div>
        </li>
        <li class="strategies__item">
          <div class="strategies__icon">
            <img src="img/strategies_icon_4.svg" alt="">
          </div>
          <div class="strategies__content">
            <p class="strategies__description">
              <span class="strategies__title">La Martingale Américaine</span>
               - La Martingale Américaine est l’une des meilleures montantes applicables sur la roulette en ligne en cas de situation déficitaire du joueur. Le principe de cette martingale est d’effacer 2 pertes à chaque gain obtenu....
            </p>
            <a href="#" class="button button--secondary strategies__button">Lire la suite</a>
          </div>
        </li>
        <li class="strategies__item">
          <div class="strategies__icon">
            <img src="img/strategies_icon_5.svg" alt="">
          </div>
          <div class="strategies__content">
            <p class="strategies__description">
              <span class="strategies__title">La Martingale Whittacker</span>
               - La Martingale Whittacker est une montante dont l’utilisation sert à limiter la parfois trop rapide progression des mises de la Martingale Classique. Cette technique présente certains avantages mais aussi contraintes que nous détaillons dans le paragraphe suivant....
            </p>
            <a href="#" class="button button--secondary strategies__button">Lire la suite</a>
          </div>
        </li>
        <li class="strategies__item">
          <div class="strategies__icon">
            <img src="img/strategies_icon_6.svg" alt="">
          </div>
          <div class="strategies__content">
            <p class="strategies__description">
              <span class="strategies__title">La Martingale de Wells</span>
               - Peu connue, la Martingale de Wells, aussi appelée « Montante de Wells », est une montante en perte. On l’utilise généralement lorsqu’on souhaite miser 5 jetons, unités ou pièces dès le début d’une partie de Roulette....
            </p>
            <a href="#" class="button button--secondary strategies__button">Lire la suite</a>
          </div>
        </li>
        <li class="strategies__item">
          <div class="strategies__icon">
            <img src="img/strategies_icon_7.svg" alt="">
          </div>
          <div class="strategies__content">
            <p class="strategies__description">
              <span class="strategies__title">Remporter un tournoi de roulettes</span>
               - A l’instar des établissements terrestres, il est maintenant possible de participer à des tournois de roulettes au sein de casinos virtuels. Si ce genre d’événements présente de nombreux avantages en termes de rémunération...
            </p>
            <a href="#" class="button button--secondary strategies__button">Lire la suite</a>
          </div>
        </li>
        <li class="strategies__item">
          <div class="strategies__icon">
            <img src="img/strategies_icon_8.svg" alt="">
          </div>
          <div class="strategies__content">
            <p class="strategies__description">
              <span class="strategies__title">La martingale de Martin Yung</span>
               - Variante du Paroli, la martingale de Martin Yung porte le même nom que son créateur. Elle fonctionne selon le mode d’une diminution continuelle du nombre de coups à jouer, en corrélation avec les éventuelles pertes enregistrées par le joueur....
            </p>
            <a href="#" class="button button--secondary strategies__button">Lire la suite</a>
          </div>
        </li>
        <li class="strategies__item">
          <div class="strategies__icon">
            <img src="img/strategies_icon_9.svg" alt="">
          </div>
          <div class="strategies__content">
            <p class="strategies__description">
              <span class="strategies__title">La Martingale Belge</span>
               - La martingale belge est la combinaison de deux autres variantes fonctionnant sur le mode de la chance simple. Son principe est l’augmentation progressive d’une unité à chaque coup perdant. L’augmentation progressive des mises se fait donc par ordre croissant (1, 2, 3, 4…etc)....
            </p>
            <a href="#" class="button button--secondary strategies__button">Lire la suite</a>
          </div>
        </li>
        <li class="strategies__item">
          <div class="strategies__icon">
            <img src="img/strategies_icon_10.svg" alt="">
          </div>
          <div class="strategies__content">
            <p class="strategies__description">
              <span class="strategies__title">La Martingale de Paroli</span>
               - Le Paroli est une technique qui vise à gagner le maximum en pariant peu. Pour y parvenir, le joueur doit tout simplement laisser sur le tapis la mise posée initialement ainsi que l’ensemble des gains obtenus depuis le début de la partie....
            </p>
            <a href="#" class="button button--secondary strategies__button">Lire la suite</a>
          </div>
        </li>
        <li class="strategies__item">
          <div class="strategies__icon">
            <img src="img/strategies_icon_11.svg" alt="">
          </div>
          <div class="strategies__content">
            <p class="strategies__description">
              <span class="strategies__title">La Martingale Hollandaise</span>
               - La Martingale Hollandaise est une méthode de jeu qu’il est toujours bon de connaître. Néanmoins, son principe est peut-être légèrement plus complexe que pour les autres martingales....
            </p>
            <a href="#" class="button button--secondary strategies__button">Lire la suite</a>
          </div>
        </li>
        <li class="strategies__item">
          <div class="strategies__icon">
            <img src="img/strategies_icon_12.svg" alt="">
          </div>
          <div class="strategies__content">
            <p class="strategies__description">
              <span class="strategies__title">La Martingale d'Alembert</span>
               - La pyramide d'Alembert fait partie des diverses catégories de martingales. C’est notamment l’une des plus faciles à mettre en œuvre. Son fonctionnement n’a en effet rien de bien compliqué...
            </p>
            <a href="#" class="button button--secondary strategies__button">Lire la suite</a>
          </div>
        </li>
        <li class="strategies__item">
          <div class="strategies__icon">
            <img src="img/strategies_icon_13.svg" alt="">
          </div>
          <div class="strategies__content">
            <p class="strategies__description">
              <span class="strategies__title">La martingale Piquemouche</span>
               - La martingale Piquemouche est une autre variante de la martingale classique. Le joueur recommence à une unité quand il gagne, mais quand il perd, il augmente sa mise d'une unité, le joueur ne double sa mise qu'après trois pertes consécutives....
            </p>
            <a href="#" class="button button--secondary strategies__button">Lire la suite</a>
          </div>
        </li>
        <li class="strategies__item">
          <div class="strategies__icon">
            <img src="img/strategies_icon_14.svg" alt="">
          </div>
          <div class="strategies__content">
            <p class="strategies__description">
              <span class="strategies__title">Les probabilités à la roulette</span>
               - Nous essaierons de vous expliquer toutes les probabilités que la balle arrive sur un numéro. Il est facile d'y calculer les probabilités et important de les comprendre lorsque vous prenez une décision avant que la balle ne soit lancée....
            </p>
            <a href="#" class="button button--secondary strategies__button">Lire la suite</a>
          </div>
        </li>
        <li class="strategies__item">
          <div class="strategies__icon">
            <img src="img/strategies_icon_13.svg" alt="">
          </div>
          <div class="strategies__content">
            <p class="strategies__description">
              <span class="strategies__title">La méthode du Piquemouche</span>
               - La martingale Piquemouche est une autre variante de la martingale classique. Le joueur recommence à une unité quand il gagne, mais quand il perd, il augmente sa mise d'une unité, le joueur ne double sa mise qu'après trois pertes consécutives....
            </p>
            <a href="#" class="button button--secondary strategies__button">Lire la suite</a>
          </div>
        </li>
        <li class="strategies__item">
          <div class="strategies__icon">
            <img src="img/strategies_icon_15.svg" alt="">
          </div>
          <div class="strategies__content">
            <p class="strategies__description">
              <span class="strategies__title">La Grande martingale</span>
               - La Grande martingale est en fait une variante de la martingale classique. La grande Martingale consiste à doubler la mise précédente en y ajoutant une unité....
            </p>
            <a href="#" class="button button--secondary strategies__button">Lire la suite</a>
          </div>
        </li>
        <li class="strategies__item">
          <div class="strategies__icon">
            <img src="img/strategies_icon_16.svg" alt="">
          </div>
          <div class="strategies__content">
            <p class="strategies__description">
              <span class="strategies__title">La martingale classique</span>
               - La martingale classique également appelé martingale de Hawks est la martingale la plus connue et la plus utilisée....
            </p>
            <a href="#" class="button button--secondary strategies__button">Lire la suite</a>
          </div>
        </li>
        <li class="strategies__item">
          <div class="strategies__icon">
            <img src="img/strategies_icon_17.svg" alt="">
          </div>
          <div class="strategies__content">
            <p class="strategies__description">
              <span class="strategies__title">Définition d'une martingale</span>
               - Pour les jeux de hasard comme le jeu de roulette par exemple, de nombreuses techniques ont été mises en place pour permettre aux joueurs de tenter de gagner le jackpot, tout en minimisant leurs pertes. Ces techniques s'appellent les martingales....
            </p>
            <a href="#" class="button button--secondary strategies__button">Lire la suite</a>
          </div>
        </li>
      </ul><!-- strategies-list --> 
    </div><!-- inner-page -->


    <?php
    echo ViewRenderer::render('footer.php', $data);
    ?>

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/common.js"></script>

  </body>
</html>