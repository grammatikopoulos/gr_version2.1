<?php
/**
 * Created by PhpStorm.
 * User: Nikiforos
 * Date: 29/08/2018
 * Time: 15:52
 */
?>

<nav class="main-nav">
    <div class="main-nav__container container">
        <button class="main-nav__mobile-button">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <div class="main-nav__block">
            <ul class="main-nav__list">
                <li class="main-nav__item main-nav__active">
                    <a class="main-nav__link" href="/">Accueil</a>
                </li>
                <li class="main-nav__item">
                    <a class="main-nav__link" href="#">Top Casino</a>
                </li>
                <li class="main-nav__item">
                    <a class="main-nav__link" href="regle-roulette.html">Règles</a>
                </li>
                <li class="main-nav__item">
                    <a class="main-nav__link" href="strategie-roulette.html">Stratégies</a>
                </li>
                <li class="main-nav__item">
                    <a class="main-nav__link" href="bonus.html">Bonus</a>
                </li>
                <li class="main-nav__item">
                    <a class="main-nav__link" href="guide.html">Guide du joueur</a>
                </li>
                <li class="main-nav__item">
                    <a class="main-nav__link" href="free.html">Jeux gratuits</a>
                </li>
            </ul>
        </div>
    </div><!-- container -->
</nav><!-- main-nav -->
