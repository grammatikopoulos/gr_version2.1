<?php
/**
 * Created by PhpStorm.
 * User: Nikiforos
 * Date: 29/08/2018
 * Time: 15:52
 */
?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $data->meta_description; ?>">
    <title><?php echo $data->meta_title; ?></title>

<!-- Fonts -->
   <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i,800" rel="stylesheet">

   <!-- CSS -->
    <link href="css/normalize.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">

  </head>
  <body>

    <header>
      <div class="container">
        <div class="header__content">
          <div class="logo">
            <a href="index.html" class="logo__link" tabindex="-1">
              <img class="logo__img" src="img/logo.svg" width="272" height="169" alt="Guide Roulette Logo">
            </a>
          </div>
        </div>
      </div><!-- container -->
  </header><!-- header -->