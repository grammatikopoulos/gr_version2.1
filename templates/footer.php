
<?php
/**
 * Created by PhpStorm.
 * User: Nikiforos
 * Date: 29/08/2018
 * Time: 15:19
 */
?>
<footer>
    <div class="container">
        <ul class="footer-nav">
            <li class="footer-nav__item">
                <a class="footer-nav__link" href="/">Accueil</a>
            </li>
            <li class="footer-nav__item">
                <a class="footer-nav__link" href="#">Top Casino</a>
            </li>
            <li class="footer-nav__item">
                <a class="footer-nav__link" href="/regle-roulette.html">Règles</a>
            </li>
            <li class="footer-nav__item">
                <a class="footer-nav__link" href="/strategies.html">Stratégies</a>
            </li>
            <li class="footer-nav__item">
                <a class="footer-nav__link" href="bonus.html">Bonus</a>
            </li>
            <li class="footer-nav__item">
                <a class="footer-nav__link" href="guide.html">Guide du joueur</a>
            </li>
            <li class="footer-nav__item">
                <a class="footer-nav__link" href="free.html">Jeux gratuits</a>
            </li>
        </ul>
        <p class="copyright">
            <a class="copyright__link" href="https://www.guide-roulette.net/">www.guide-roulette.net</a>
        </p>
    </div><!-- container -->
</footer><!-- footer -->
