<?php
/**
 * Created by PhpStorm.
 * User: Nikiforos
 * Date: 29/08/2018
 * Time: 14:24
 */
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $data->meta_description; ?>">
    <title><?php echo $data->meta_title; ?></title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet">

    <!-- CSS -->
    <link href="css/normalize.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">

</head>
<body>

<header>
    <div class="container">
        <div class="header__content">
            <div class="logo">
                <img class="logo__img" src="img/logo.svg" width="272" height="169" alt="Guide Roulette Logo">
            </div>
        </div>
    </div><!-- container -->
</header><!-- header -->

<nav class="main-nav">
    <div class="main-nav__container container">
        <button class="main-nav__mobile-button">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <div class="main-nav__block">
            <ul class="main-nav__list">
                <li class="main-nav__item">
                    <a class="main-nav__link" href="/">Accueil</a>
                </li>
                <li class="main-nav__item">
                    <a class="main-nav__link" href="#">Top Casino</a>
                </li>
                <li class="main-nav__item main-nav__active">
                    <a class="main-nav__link" href="regle-roulette.html">Règles</a>
                </li>
                <li class="main-nav__item">
                    <a class="main-nav__link" href="strategie-roulette.html">Stratégies</a>
                </li>
                <li class="main-nav__item">
                    <a class="main-nav__link" href="bonus.html">Bonus</a>
                </li>
                <li class="main-nav__item">
                    <a class="main-nav__link" href="guide.html">Guide du joueur</a>
                </li>
                <li class="main-nav__item">
                    <a class="main-nav__link" href="free.html">Jeux gratuits</a>
                </li>
            </ul>
        </div>
    </div><!-- container -->
</nav><!-- main-nav -->

<section class="about">
    <div class="container">
        <div class="about__content">
            <h1 class="about__title secondary-title--decorated"><?php echo $data->main_title; ?></span></h1>
            <div class="text-block">
                <p><?php echo $data->main_text; ?></p>
            </div>
        </div>
    </div><!-- container -->
</section><!-- about -->


<?php
echo ViewRenderer::render('footer.php', $data);
?>

<!-- Scripts -->
<script src="js/jquery.min.js"></script>
<script src="js/common.js"></script>

</body>
</html>