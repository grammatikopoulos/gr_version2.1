
<!DOCTYPE html>
<html lang="fr">

<?php
echo ViewRenderer::render('header.php', $data);
?>

<nav class="main-nav">
    <div class="main-nav__container container">
        <button class="main-nav__mobile-button">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <div class="main-nav__block">
            <ul class="main-nav__list">
                <li class="main-nav__item main-nav__active">
                    <span class="main-nav__link">Accueil</span>
                </li>
                <li class="main-nav__item">
                    <a class="main-nav__link" href="#">Top Casino</a>
                </li>
                <li class="main-nav__item">
                    <a class="main-nav__link" href="regle-roulette.html">Règles</a>
                </li>
                <li class="main-nav__item">
                    <a class="main-nav__link" href="strategie-roulette.html">Stratégies</a>
                </li>
                <li class="main-nav__item">
                    <a class="main-nav__link" href="bonus.html">Bonus</a>
                </li>
                <li class="main-nav__item">
                    <a class="main-nav__link" href="guide.html">Guide du joueur</a>
                </li>
                <li class="main-nav__item">
                    <a class="main-nav__link" href="free.html">Jeux gratuits</a>
                </li>
            </ul>
        </div>
    </div><!-- container -->
</nav><!-- main-nav -->

<section class="maintext">
    <div class="container">
        <div class="about__content">
            <h1 class="about__title secondary-title--decorated"><?php echo $data->main_title; ?></span></h1>
            <div class="text-block">
                <p><?php echo $data->main_text; ?></p>
            </div>
        </div>
    </div><!-- container -->
</section><!-- maintext -->



<section class="subscribe">
    <div class="container">
        <div class="subscribe-caption">
            <h2 class="subscribe__title secondary-title--decorated">Newsletter</h2>
        </div>
    </div><!-- subscribe-caption -->
    <div class="subscribe__content">
        <div class="container">
            <form class="subscribe-form">
                <div class="subscribe-form__content">
                    <div class="subscribe-form__block">
                        <div class="subscribe-form__field subscribe-form__name">
                            <label for="s-name" class="name-label" aria-label="Name"></label>
                            <input class="subscribe-form__input" type="text" id="s-name" placeholder="nom">
                        </div>
                    </div>
                    <div class="subscribe-form__block">
                        <div class="subscribe-form__field subscribe-form__email">
                            <label for="s-email" class="email-label" aria-label="Email"></label>
                            <input class="subscribe-form__input" type="email" id="s-email" placeholder="E-mail">
                        </div>
                        <div class="subscribe-form__checkbox">
                            <label for="s-check" class="checkbox-label" aria-label="politique de confidentialité">
                                <input type="checkbox" id="s-check">
                                J'ai lu et j'accepte la <a class="subscribe-form__link" href="#">politique de confidentialité</a>.
                            </label>
                        </div>
                    </div>
                    <div class="subscribe-form__button">
                        <button type="submit" class="button button--secondary">S’inscrire</button>
                    </div>
                </div>
            </form>
        </div>
    </div><!-- subscrine-content -->
</section><!-- subscribe -->

<section class="site_main_2">
    <div class="container">
        <div class="about__content">
           <h1 class="about__title secondary-title--decorated"><?php //echo $data->main_title; ?></span></h1>
            <div class="text-block">
                <p><?php echo $data->site_main_2; ?></p>
            </div>
        </div>
    </div><!-- container -->
</section><!-- about -->

</section><!-- site_main_2 -->


<?php
    echo ViewRenderer::render('footer.php', $data);
?>

<!-- Scripts -->
<script src="js/jquery.min.js"></script>
<script src="js/common.js"></script>

</body>
</html>