<?php
/**
 * Created by PhpStorm.
 * User: Nikiforos
 * Date: 28/08/2018
 * Time: 17:54
 */

if (strpos($_SERVER['HTTP_HOST'],".")!==false) {
    define('DB_SERVER', 'localhost');
    define('DB_SERVER_USERNAME', 'larengod_1');
    define('DB_SERVER_PASSWORD', 'Qw2334bV');
    define('DB_DATABASE', 'larengod_1');
} else {
    define('DB_SERVER_USERNAME', 'root');
    define('DB_SERVER_PASSWORD', '123');
    define('DB_SERVER', 'localhost');
    define('DB_DATABASE', 'bonuscas_ab89anjjek');
}

class ContentPage {

    public $page_url;
    public $meta_title;
    public $meta_description;
    public $menu_title;
    public $type_admin;
    public $main_title;
    public $main_text;
    public $type;
    public $site_main_2;

}

class ContentPageRepository {

    function getContentPage($pageUrl)
    {
        $page = new ContentPage();
        $conn = new mysqli(DB_SERVER, DB_SERVER_USERNAME, DB_SERVER_PASSWORD, DB_DATABASE);
        //mysqli_set_charset($conn,"utf8");
        $conn->set_charset("utf8");
    // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        $sql = "select * from gr_site where page_url='$pageUrl' LIMIT 1";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $page->page_url = $row["page_url"];
                $page->meta_title = $row["meta_title"];
                $page->meta_description = $row["meta_description"];
                $page->menu_title = $row["menutitle"];
                $page->type_admin = $row["type_admin"];
                $page->main_title = $row["maintitle"];
                $page->main_text = $row["maintext"];
                $page->type = $row["type"];
                $page->site_main_2 = $row["site_main_2"];

            }
        }
        $conn->close();
        return $page;
    }
}