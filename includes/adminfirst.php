<?php 
if (!isset($_SERVER['HTTPS']) OR $_SERVER['HTTPS']=="off") $site_protocol="http://"; else $site_protocol="https://";

$citiestoblock=array("franconville","franconville la garenne","saint-leu-la-forêt","le plessis-bouchard","delacroix","mare des noues","saint-prix","fontaine bertin","beauchamp","cormeilles-en-parisis","argenteuil","ermont","eaubonne","sannois","montigny-lès-cormeilles","la frette-sur-seine","herblay","soisy-sous-montmorency","montmorency","pierrelaye","taverny","saint-gratien","asnières","vert","clichy","nanterre","garges-lès-gonesse","la garenne-colombes","saint-cloud","nanterre","sarcelles","omerville","viesly","hazebrouck","le quesnoy-en-artois","villejuif","annoeullin","fontenay-le-fleury","asnieres-sur-seine","vert","clichy","neuilly-sur-seine","jouy-le-moutier","melun");
if (!(in_array(date("N"),array(6,7)) OR date("G")<8 OR date("G")>=18)) $citiestoblock[]="paris";
$ipblocklist=array("82.124.88","86.249.128","83.204.187","83.204.239","92.132.156","92.132.166","92.132.218","92.132.233");

if (preg_match("@([^\?]+)(.*)@",$_SERVER['REQUEST_URI'],$matches)) {
	$s1111= preg_replace("@^(/[^/\.]+)$@","$1/",$matches[1],-1,$count);
	if ($count) {
		header("Location: $site_protocol{$_SERVER['HTTP_HOST']}$s1111{$matches[2]}",TRUE,301);
		exit;
	}
}

$_SERVER['REDIRECT_URL']=preg_replace("@\?.*$@","",$_SERVER['REQUEST_URI']);
$alt_ips=array(
"bl"=>"fr",
"gf"=>"fr",
"gp"=>"fr",
"mf"=>"fr",
"mq"=>"fr",
"nc"=>"fr",
"pf"=>"fr",
"pm"=>"fr",
"re"=>"fr",
"tf"=>"fr",
"wf"=>"fr",
"yt"=>"fr"
);

if (strpos($_SERVER['HTTP_HOST'],".")===false) $localmode=true;

mysqli_query($mysqlilink,"set time_zone = '+0:00'");

$sitedata=sitedata();

if (file_exists("{$sitedata['short']}_301.txt")) {
	$site301=file_get_contents("{$sitedata['short']}_301.txt");
	preg_match_all("@^(.+)\=>(.+)\r*$@m",$site301,$matches301);
	$cururllower=strtolower($_SERVER['REQUEST_URI']);
	foreach ($matches301[1] as $ind301=>$result301) {
		$result301lower=strtolower($result301);
		if ($result301lower==$cururllower) {
			switch ($matches301[2][$ind301]) {
				case "410":
					header($_SERVER["SERVER_PROTOCOL"]." 410 Gone"); 
					break;
				default:	
					header("Location: {$matches301[2][$ind301]}",TRUE,301);
			}
			die;
		}
	}
}

if ($sitedata['wholesite301']) {
	header("Location: {$sitedata['wholesite301']}",TRUE,301);
	die;
}

$sitetypegeneral=sitetypegeneral();

$master_short=$sitedata['samerootas'] or $master_short=$sitedata['parent_site'] or $master_short=$sitedata['short'];

// do redirection to https if the site is set as https
if (!$localmode and (!isset($_SERVER['HTTPS']) OR $_SERVER['HTTPS']=="off") and $sitedata['protocol']=="https") {
	$urinq=preg_replace("@\?.*$@","",$_SERVER['REQUEST_URI']);
	if ($_SERVER['QUERY_STRING']) $uriquery="?{$_SERVER['QUERY_STRING']}";
	header("Location: https://{$_SERVER['HTTP_HOST']}$urinq$uriquery",TRUE,301);	
}

if ($master_short!="bcel" AND preg_match("@^/robots\.txt$@",$_SERVER['REQUEST_URI'])) {
	if (file_exists("robots.txt")) $robotstext=file_get_contents("robots.txt");
	else $robotstext=<<<A
User-agent: *
Disallow: /go/
A;
	$robotstext.="\r\nSitemap: $site_protocol{$_SERVER['HTTP_HOST']}/sitemap.xml";
	header("Content-Type: text/plain"); 
	echo $robotstext;
	exit;
}

if (!$localmode) {
	$remoteip=$_GET['testip'] OR $remoteip=$_SERVER['REMOTE_ADDR'];
	if (substr($sitedata['host_id'],0,10)=="infomaniak") {
		$webparent=preg_replace("@/web.*$@","",$_SERVER['DOCUMENT_ROOT']);
		require_once "$webparent/web/geoip/checkip.php";
		if (should_block($remoteip)) exit;
		$iphaschecked=true;
	} elseif (in_array($sitedata['short'],array("clf","cfo","cs"))) {
		require_once $_SERVER['DOCUMENT_ROOT']. "/geoip/checkip.php";
		if (should_block($remoteip)) exit;
		$iphaschecked=true;
	} elseif (in_array($sitedata['short'],array("cl"))) {
		$ipcountry=file_get_contents_https("https://www.casinoonlinefrancais.info/ipcountry.php?remoteip=$remoteip");
		$iphaschecked=true;
	}
}

if (substr($sitedata['host_id'],0,10)=="infomaniak") $use_univ_ipcountry=1;

if ($ismobilephone) {
	$freegamenewvar=array("folder"=>"t","maindb"=>"freegamenewtouch","datadb"=>"freegamenew_mobile","src"=>"freegamenewtouch_src");
	$platform="mobile";
}
else {
	$freegamenewvar=array("folder"=>"f","maindb"=>"freegamenew","datadb"=>"freegamenew_pc","src"=>"freegamenew_src");
	$platform="pc";
}
if (preg_match("@/univemail\.html$@",$_SERVER['REQUEST_URI'])) {
	if (!$_POST) die;

	$_POST=univ_stripslashes_deep($_POST);

	$sitename=$sitedata['url'];
	$name= $_POST['name'];
	$mailTo = "{$sitename} <website@bluewindowltd.com>";
	$mailFrom = $_POST['email'];
	$mailFrom = $name . " <" . $mailFrom . ">";
	$message = $_POST['message'];
	$message = preg_replace("/(.+)/", "$1<br /><br />", $message);
	
	$subject = $_POST['subject'];
	
	// subject
	if (!$subject) $subject = "A message regarding $sitename";
	
	// message
	$message = '
<html>
<head>
  <title>&nbsp;</title>
</head>
<body style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#222222;">
  <p>' . $message . '</p>
<br /><br /><br />
</body>
</html>
';

	// To send HTML mail, the Content-type header must be set
	$headers  = 'MIME-Version: 1.0' . "\n";
	$headers .= 'Content-type: text/html; charset="utf-8";' . "\n";
	$headers .= 'Content-Transfer-Encoding: 8bit' . "\n";
	
	// Additional headers
	$headers .= 'To: ' . $mailTo . "\n";
	$headers .= 'From: ' . $mailFrom . "\n";
	$headers .= "\n";
	
	// Mail it
	mail($mailTo, $subject, $message, $headers);
	die;
}
if (preg_match("@/sitemap\.html$@",$_SERVER['REQUEST_URI'])) {
	if (!in_array($master_short,array("bcel","clf"))) {
		$maintable=$sitedata['db_tables'];
		if ($master_short=="tcc") $maintable="tcc_site";

		$is_univ_sitemap=1;
		if (!$site_table_criteria) $site_table_criteria=site_table_criteria();
		$sitemaphtml=mres(af_listpagesofsite());
		$querysitemap="select id from $maintable{$GLOBALS['previewtail'][$maintable]} where page_url = '/sitemap.html'$site_table_criteria and theorder>0";
		$resultsitemap=mnq($querysitemap);
		if (mnr($resultsitemap)) {
			$rsq=mfs($resultsitemap);
			$qsp="update $maintable{$GLOBALS['previewtail'][$maintable]} set maintext = '$sitemaphtml' where id={$rsq['id']}";
			mnq($qsp);
		} else {
			if ($site_table_criteria) {
				$addccc=",".strtr($site_table_criteria,array("AND "=>"","IS NULL"=>"= NULL"));
			}
			$metatitle_field=metatitle_field($maintable);
			$qsp="insert into $maintable{$GLOBALS['previewtail'][$maintable]} set
page_url='/sitemap.html',maintitle='SITE MAP',maintext='$sitemaphtml',$metatitle_field='Site Map',theorder=1$addccc
";
			mnq($qsp);
		}
		
	}
}

if (preg_match("@/sitemap\.xml$@",$_SERVER['REQUEST_URI'])) {
	if (function_exists("sitemapxmlbcel")) sitemapxmlbcel();
	elseif (function_exists("sitemapxmlclf")) sitemapxmlclf();
	elseif (function_exists("sitemapxmltcc")) sitemapxmltcc();
	else sitemapxml();
	exit;
}

if (preg_match("@^{$sitedata['root']}adminpreview\.html@",$_SERVER['REQUEST_URI'])) {
	array_walk_recursive($_POST,"admin_mystripslashes");
	array_walk_recursive($_POST,"admin_mytrim");
	$previewmode=true;
	$tablename=$_POST['tablename'];
	$draftid=$_POST['draftid'];
	if (!$_POST['table_id']) $newarticle=1;
	$previewtail[$tablename]="_temp".time().rand(0,9999999);

	mnq("CREATE TEMPORARY TABLE {$tablename}{$previewtail[$tablename]} LIKE $tablename");

		$u=array();
		$query="SHOW COLUMNS FROM {$tablename}{$previewtail[$tablename]}";
		$result=mnq($query);
		while ($r=mfs($result)) {
			$u[$r['Field']]=$_POST['f_'.$r['Field']];;
		}
		
		if ($newarticle) {
			switch ($tablename) {
				case "news":
					$u['page_url']="/news/1.html";
					break;
				default:
					if (!$u['page_url']) $u['page_url']="/1.html";	
			}
		}
		
		// images and quote need to change
		
		if (array_key_exists("quotes",$u)) $u['quotes']=getquotesfromarray($_POST);
		if (array_key_exists("images",$u)) $u['images']=getimagesfromarray($_POST,$u['images']);
		switch ($_POST['imgselect0']) {
			case "":
				if(array_key_exists('image',$u)) $u['image']="";
				elseif(array_key_exists('pic_url',$u)) $u['pic_url']="";
				break;
			case "ori":
				break;
			default:	
				if(array_key_exists('image',$u)) $u['image']="~~temp~~$draftid~{$_POST['imgselect0']}";
				elseif(array_key_exists('pic_url',$u)) $u['pic_url']="~~temp~~$draftid~{$_POST['imgselect0']}";
		}
		
		if ($_POST['imgwidth0'] and array_key_exists("picmain_width",$u)) 
			$u['picmain_width']=$_POST['imgwidth0'];
		if ($_POST['imgheight0'] and array_key_exists("picmain_height",$u)) 
			$u['picmain_height']=$_POST['imgheight0'];

		$_SERVER['REQUEST_URI']=$u['page_url'];
		$_SERVER['REDIRECT_URL']=$u['page_url'];
		
	// put data into the temp table
		$ff=array();
		//unset($u['id']);
		$u['id']=1;
		if ($sitedata['parent_site']) $u['subsite']=substr($sitedata['root'],0,-1);
		foreach ($u as $ui=>$un) {
			if ($un) $ff[]="`$ui`='".mres($un)."'";
			else $ff[]="`$ui`=NULL";
		}
		$ffs=implode(",",$ff); 

		$q="insert into {$tablename}{$previewtail[$tablename]} set $ffs";
		if (!mnq($q)) die("Error updating DB: $q ". mer());	

} elseif (preg_match("@^\/admin\.html@",$_SERVER['REQUEST_URI'])) {
	if ($_GET['updatemd5']) {
		$update_md5=update_md5($_GET['updatemd5']);
		header("Content-Type: text/plain");
		echo $update_md5;
	} elseif ($_GET['updatefreegameimages']) {
		$query="select t1.* from freegamenew as t1 left join freegamenew_pc as t2 on t1.id_unique=t2.id_unique where t2.status=1";
		$result=mnq($query);
		$gameswithimages=array();
		while ($r=mfs($result)) {
			if ($r['md5']) {
				if (@md5_file($_SERVER['DOCUMENT_ROOT']."/img/f/{$r['id_unique']}.jpg")!=$r['md5']) {
					$f=@file_get_contents_https("http://www.bonus-casino-en-ligne.info/img/f/{$r['id_unique']}.jpg");
					if ($f) {
						if (file_put_contents($_SERVER['DOCUMENT_ROOT']."/img/f/{$r['id_unique']}.jpg"
							,$f)!==false) {
							$gameswithimages[]=$r['id_unique'];
						}
					}
				} else $gameswithimages[]=$r['id_unique'];
			}
		}

		$gameswithimages=implode(",",$gameswithimages);
		file_put_contents($_SERVER['DOCUMENT_ROOT']."/freegamenew.txt",$gameswithimages);
		
		$query="select t1.* from freegamenewtouch as t1 left join freegamenew_mobile as t2 on t1.id_unique=t2.id_unique where t2.status=1";
		$result=mnq($query);
		$gameswithimages=array();
		while ($r=mfs($result)) {
			if ($r['md5']) {
				if (@md5_file($_SERVER['DOCUMENT_ROOT']."/img/t/{$r['id_unique']}.jpg")!=$r['md5']) {
					$f=@file_get_contents_https("http://www.bonus-casino-en-ligne.info/img/t/{$r['id_unique']}.jpg");
					if ($f) {
						if (file_put_contents($_SERVER['DOCUMENT_ROOT']."/img/t/{$r['id_unique']}.jpg"
							,$f)!==false) {
							$gameswithimages[]=$r['id_unique'];
						}
					}
				} else $gameswithimages[]=$r['id_unique'];
			}
		}
		
		$gameswithimages=implode(",",$gameswithimages);
		file_put_contents($_SERVER['DOCUMENT_ROOT']."/freegamenewtouch.txt",$gameswithimages);
		
		echo "done";
	}
	
	die;	
}

if ($_GET['toplist']) {
	$remoteip=$_SERVER['REMOTE_ADDR']; 
	$width=$_GET['width'];
	$cat=$_GET['cat'];
	$s=@file_get_contents("http://www.thecasinocity.com/topcasinosnew.php?width=$width&remoteip=$remoteip&cat=$cat");
	if ($s) {
		if (substr($s,-10, 6)!="cready") $s="";
	}
	if ($s) echo $s; else echo <<<A
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
</body>
</html>
A;
	exit;	
}

if (in_array($sitedata['using_template'],array("1","2","3")) or $use_univ_ipcountry) univ_ipcountry();


//functions begin

function processcssfiles($mainscss,$mobilecss="",$mainscssreplace="",$mobilecssreplace="") {
	global $ismobilephone;
	$cssquote=preg_quote($mainscss);
	if (preg_match("@^$cssquote@",$_SERVER['REQUEST_URI'])) {
		header("Content-type: text/css");
		$css= file_get_contents($_SERVER['DOCUMENT_ROOT'].$mainscss);
        if ($mainscssreplace) $css=strtr($css,$mainscssreplace);
		if ($ismobilephone) {
        	$mcss=file_get_contents($_SERVER['DOCUMENT_ROOT'].$mobilecss);
            if ($mobilecssreplace) $mcss=strtr($mcss,$mobilecssreplace);
        	$css.= $mcss;
		}
		// Remove comments
		$css = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css);
		// Remove space after colons
		$css = str_replace(': ', ':', $css);
		// Remove whitespace
		$css = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $css);
	
		echo $css;
		exit;
	}
}
function showthisgamenew($finalwidth) {
	global $sitelan; global $site; global $ismobilephone;
	$site['address']=trim($site['address']);
	
	if (substr($site['address'], 0, 1)!="<") $useframe=true;
	if (!$site['aspectratio']) $site['aspectratio']=$site['aspectratiodefault'];
	preg_match("@(\d+)x(\d+)@i",$site['aspectratio'],$matches);
	if ($matches and (int)$matches[1]>0 and (int)$matches[2]>0) {
		$realwidth=(int)$matches[1];
		$realheight=(int)$matches[2];
	} else {
		$realwidth=800;
		$realheight=600;
	}

	switch ($site['template']) {
		case 2: //tropezia
		// 800:583 crop bottom
		// for game provided by tropezia palace like http://affiliates.tropeziapalace.com/game/
				$divheight=(int) ($realheight*$finalwidth/$realwidth);
				$framewidth=$finalwidth;
				$frameheight=(int) (($realheight+95)*$finalwidth/$realwidth); // crop 95px at the bottom
				$site['address']=preg_replace("@(http://affiliates\.tropeziapalace\.com/game/\d+/default)/\d+/@", "$1/$framewidth/", $site['address']);
			break;
		case 9: // https://nogs-gl.nyxinteractive.eu/game/?
		// 16:9
				$divheight=(int) ($realheight*$finalwidth/$realwidth);
				$framewidth=$finalwidth;
				$frameheight=(int) ($realheight*$finalwidth/$realwidth);
			break;
		case 5: //solera
		//rival   800x600+37   crop bottom 37
				$divheight=(int) ($realheight*$finalwidth/$realwidth);
				$framewidth=$finalwidth;
				$frameheight=(int) ($realheight*$finalwidth/$realwidth) + 37;
				$site['address']=preg_replace("@windowHeight=\d+&windowWidth=\d+@", "windowHeight=$frameheight&windowWidth=$framewidth", $site['address'] );
			break;
		default:
			$divheight=(int) ($realheight*$finalwidth/$realwidth);
			$framewidth=$finalwidth;
			$frameheight=(int) ($realheight*$finalwidth/$realwidth);
	
			if ($site['template']==10) 
				$site['address']=preg_replace("@width=\"\d+px\" height=\"\d+px\"@", "width=\"{$framewidth}px\" height=\"{$frameheight}px\"", $site['address']);
			if ($site['template']==1)
				$site['address']=preg_replace("@(http\:\/\/.+/.+/.+/.+/.+)/\d+?@U","$1/$finalwidth",$site['address']);
	}

	$js.= <<<A
	<div style='overflow:hidden;height:{$divheight}px;width:{$framewidth}px;float:left;'>
A;
	if ($useframe) {
		if ($load_by_js) {
			$js.=<<<A
		<iframe$siteemovetop class='freegame_load_by_js' data-src='{$site['address']}' src='' width={$framewidth}px height={$frameheight}px scrolling=no frameborder=no>
		</iframe>
A;
		} else {
			$site['address']=htmlspecialchars($site['address']);
			$js.=<<<A
		<iframe$siteemovetop src="{$site['address']}" width={$framewidth}px height={$frameheight}px scrolling=no frameborder=no>
		</iframe>
A;
		}
		
	}
	else
	$js.="<div$siteemovetop style='width:{$framewidth}px; height:{$frameheight}px;'>{$site['address']}</div>";
	
	$js.=<<<A
	</div>
A;
	return $js;	
	
}
function addboxes($element) {
	$s=$element['maintext'];	
	
	if ($element['textinbox'][0]) {
		$s=preg_replace("#(.+)(^.+<h2>)#ism", "$1*(r)e1*$2", $s);
	}
	if ($element['textinbox'][1]) {
		$s=preg_replace("#(.+</h2>\r\n)#ism", "$1*(l)p1*", $s);
	}

	return $s;
	
}
function issubtitle($s) {
		$nottitle=false;
		$s=trim($s);
	
		$st=preg_split("@<b>(.*)</b>@U", $s, -1);
		
		foreach ($st as $stt) {
			if (strlen(trim($stt))>0) {
				$nottitle=true;
				break;
			}
		}
		return !$nottitle;
}
function tagb2h($s, $h="h2") {
	
	$ss=explode("\r\n", $s);
	foreach ($ss as &$s) {
		
		if (issubtitle($s)) {
			$s=strtr($s, array("<b>"=>"","</b>"=>"","<B>"=>"","</B>"=>""));
			$s="<$h>$s</$h>";		
		}
	}
	
	return implode("\r\n",$ss);
}
function addkeywords($s, $tablename, $fieldname="maintext") {
	return $s;
}
function modifykeywords() {
	global $keywords;	
	global $keywordsautoupdate;

	$final=array();
	
	$ee=explode("\n", $keywords);
	foreach ($ee as $e) {

		preg_match("@^(.*)=>(.+)@", $e, $matches);
		if ($matches[1]) {
			$e=trim($matches[1]);
			$link=trim($matches[2]);
		} else $link="/";
		$e=trim(strtolower($e));
		if ($e) {
			$kqm[$link][]=$e;
		}
	}
	$kqm=super_unique($kqm);
	
	foreach ($keywordsautoupdate as $tablename=>$queryandfields) {
	
		$query=$queryandfields[0];
		$result=mnq($query);
		while ($r=mfa($result)) {
			if (!$kqm) break 2;
 			foreach ($queryandfields[1] as $fieldname) {
				$s[$fieldname]=preg_replace("@<a .*/a>|<img .*>|<script .*/script>|<h2.*/h2>@isU", "", $r[$fieldname]);
            }
            
            foreach ($kqm as $kkey => &$kq) {
                foreach($kq as $i=>$q) {
                    $qq=preg_quote($q);
                    foreach ($s as $fn=> $si) {
						$si=preg_replace("@(\PL|^|')($qq)(\PL|$|')@i", "***", $si, 1, $count);
						if ($count) {
							if ($f[$q][0]) {
							//remove it
								unset($kq[$i]);
							} else {
							// move to the end, serach for another time
								$v=$kq[$i];
								unset($kq[$i]);
								$kq[]=$v;
							}
							
							$f[$q][]=$r['id'];
							$final[$tablename][$r['id']][$fn][]=array($q, $kkey);
							$finalstring.=calcurlof($tablename, $r['id'], $q, $kkey);
							break 2;
						}
                    }
                }
            }
		}
	}
		
		
	$ttt=var_export($final, true);

	$fp = fopen('k_eywords.dat', 'w');
	if (fwrite($fp, $ttt)===false) $c= "ERROR updating site"; else $c="DONE" . $finalstring . "FINALSTRINGDONE";
	fclose($fp);

	return $c;

}
function calcurlof($tablename, $id, $text, $target) {
	global $urlstructure;
	if (!$urlstructure[$tablename]) return;
	$query="select * from $tablename where id=$id";
	$result=mnq($query);
	if (mnr($result)) {
		$r=mfa($result);
		eval("\$lk=\"{$urlstructure[$tablename]}\";");
		if ($target!="/") $text.="=>$target";
		
		$v= "<a target='_blank' href='http://{$_SERVER['HTTP_HOST']}/$lk'>$text</a><br />";
		$v=preg_replace("@(http://.+)(//)@", "$1/", $v);
		return $v;
	}
		
}
function normaliza_4filenamechange ($string){ 
    $a = ' ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝàáâãäåæçèéêëìíîïðñòóôõöøüùúûýýþÿŔŕĂăȘșŞşȚțŢţŭĕĭ?\'"'; 
    $b = '-aaaaaaaceeeeiiiidnoooooouuuuyaaaaaaaceeeeiiiidnoooooouuuuyybyRraassssttttuei---'; 
    $string = utf8_decode($string);     
    $string = strtr($string, utf8_decode($a), $b); 
    $string = strtolower($string); 
    return utf8_encode($string); 
}
function filenamechange ($string) {
	$stt=normaliza_4filenamechange(trim($string));
	$stt=strtr($stt, array("("=>"", ")"=>"", "."=>"", "!"=>"", ":"=>"", "&"=>"and", "%"=>"", "ca\$her"=>"casher"));

	$stt=strtr($stt, array("--"=>"-"));
    while (substr($stt, -1)=="-") $stt=substr($stt, 0, -1);
	return $stt;
}
function super_unique($array) {
	if (!$array) return;
	$array=array_map("array_unique", $array);
	return sortbylen($array);
 }
function sortbylen($a) {
	foreach ($a as &$aa) {
		$b=array();
		foreach ($aa as $ak) {
			$b[]=strlen(trim($ak));
		}
		array_multisort($b, SORT_DESC, $aa);
	}
	return $a;
		
}
function info_for_logo_image_type_general($filetype) {
	switch ($filetype) {
		case "gif150x60":
			$typesallowed[]="gif";
			$path[]="/img/casinologo";
			$typesallowed[]="png";
			$path[]="/img/bmlogo";
			$typesallowed[]="png";
			$path[]="/img/pokerlogo";
			break;
		case "png150x60lightbg":
			$typesallowed[]="png";
			$path[]="/img/logo";
			$typesallowed[]="png";
			$path[]="/img/blogo";
			$typesallowed[]="png";
			$path[]="/img/plogo";
			break;
		case "png150x60darkbg":
			$typesallowed[]="png";
			$path[]="/img/logo_1";
			$typesallowed[]="png";
			$path[]="/img/blogo_1";
			$typesallowed[]="png";
			$path[]="/img/plogo_1";
			break;
		case "png250x100lightbg":
			$typesallowed[]="png";
			$path[]="/img/logo250";
			$typesallowed[]="png";
			$path[]="/img/blogo250";
			$typesallowed[]="png";
			$path[]="/img/plogo250";
			break;
		case "png250x100darkbg":
			$typesallowed[]="png";
			$path[]="/img/logo250_1";
			$typesallowed[]="png";
			$path[]="/img/blogo250_1";
			$typesallowed[]="png";
			$path[]="/img/plogo250_1";
			break;
		case "screen1":
			$typesallowed[]="png";
			$path[]="/img/casinoscreen1";
			$typesallowed[]="png";
			$path[]="/img/bmscreen1";
			$typesallowed[]="png";
			$path[]="/img/pokerscreen1";
			break;
		case "screen2":
			$typesallowed[]="jpg";
			$path[]="/img/casinoscreen2";
			$typesallowed[]="jpg";
			$path[]="/img/bmscreen2";
			$typesallowed[]="jpg";
			$path[]="/img/pokerscreen2";
			break;
		case "screen3":
			$typesallowed[]="jpg";
			$path[]="/img/casinoscreen3";
			$typesallowed[]="jpg";
			$path[]="/img/bmscreen3";
			$typesallowed[]="jpg";
			$path[]="/img/pokerscreen3";
			break;
		case "screen4":
			$typesallowed[]="jpg";
			$path[]="/img/casinoscreen4";
			break;
		default:return;
	}        
	$a=array();
	foreach ($path as $ind=> $p) {
		if (file_exists($_SERVER['DOCUMENT_ROOT'].$p)) {
			$a[]=array($typesallowed[$ind],$path[$ind]);
		}
	}
	return $a;
}
function update_md5($filetype) {
	$filetypeinfos=info_for_logo_image_type_general($filetype);
	if (!$filetypeinfos) die("Wrong file type");

	foreach ($filetypeinfos as $filetypeinfo) {
		$typesallowed=$filetypeinfo[0];
		$path=$filetypeinfo[1];
	
		$e=scandir($_SERVER['DOCUMENT_ROOT'].$path);
	
		foreach ($e as $ee) {
			if (substr($ee,-4)==".$typesallowed") {
				$md=md5_file($_SERVER['DOCUMENT_ROOT'].$path."/".$ee);
				$s.= $ee." ";
				$s.= $md."\r\n";
			}
		}
	}
	
	file_put_contents($_SERVER['DOCUMENT_ROOT']."/$filetype.txt",$s);
	return $s;
}
if (!function_exists("softwarenameofid")) {
	function softwarenameofid($type="casino") {
		$a=array();
		switch ($type) {
			case "casino":
				$query="select * from bcel_softwarenames where not software_id IS NULL";
				$result=mnq($query);
				while ($r=mfa($result)) {
					$a[$r['software_id']]=$r['software_name'];
				}
				break;
			case "poker":
				$query="select * from manage_pokersoftware where not poker_software_id IS NULL";
				$result=mnq($query);
				while ($r=mfa($result)) {
					$a[$r['poker_software_id']]=$r['poker_software_name'];
				}
				break;
		}
		return $a;
	}
}
if (!function_exists("jurisdiction_trans")) {
	function jurisdiction_trans($t) {
		global $jurisdiction_dict; global $sitelan;
		if (!$jurisdiction_dict) {
			$jurisdiction_dict=array();
			$query="select * from bcel_jurisdiction where nullif(jurisdiction_id,'') is not null";
			$result=mnq($query);
			while ($r=mfs($result)) {
				$jurisdiction_dict[$r['jurisdiction_id']]=$r[$sitelan] or $jurisdiction_dict[$r['jurisdiction_id']]=$r['jurisdiction_name'];
			}
		}

		$e=explode(",",$t);
		$a=array();
		foreach ($e as $ee) {
			if ($ee) {
				if ($jurisdiction_dict[$ee]) $a[]=$jurisdiction_dict[$ee];
			}
		}
		return implode(", ",$a);
	}
}
if (!function_exists("calc_bonus_match")) {
	function calc_bonus_match($max_bonus, $deposit4maxbonus) {
		$unit=substr($max_bonus,-3);
		preg_match("@(\d+)$unit@",$deposit4maxbonus,$matches);
		if ($matches[1] and (int)$matches[1]>0) {
			return round((int)$max_bonus/$matches[1]*100);
		}
	}
}
if (!function_exists("calc_firstdepositbonus_and_bonusmatch")) {
	function calc_firstdepositbonus_and_bonusmatch() {
		global $sitelan; global $exclusive_bonus_allowed; global $sitetype;
		if (!$sitetype) $sitetype="casino";
		$idname=array("casino"=>"casino_id","poker"=>"poker_id","sport"=>"bm_id");
		$bonuses_new_name=array("casino"=>"bonuses_new","poker"=>"bonuses_new_pokerrooms","sport"=>"bonuses_new_bookmakers");

		$the_history=array(); $a=array();
		if (!$exclusive_bonus_allowed) $addexc=" AND IFNULL(is_exclusive_bonus,0) = 0";
		$query="select * from {$bonuses_new_name[$sitetype]} where 
			   onlyfornotfor ='onlyfor $sitelan'
			   AND {$idname[$sitetype]} IS NOT NULL 
			   and bonus_type='Welcome' 
			   and IFNULL(nth,0) < 2 
			   and IFNULL(deposit4maxbonus,0)>0 
			   $addexc
			   ORDER BY {$idname[$sitetype]}, max_bonus/deposit4maxbonus DESC, max_bonus DESC";
		$result=mnq($query);
		while ($r=mfa($result)) {
			if (!in_array($r[$idname[$sitetype]], $the_history)) {
				$the_history[]=$r[$idname[$sitetype]];
				
				$mbmb=currencychange($r['max_bonus'],"",1);
				$pcpc=calc_bonus_match($mbmb,$r['deposit4maxbonus']);
				$a[$r[$idname[$sitetype]]]=array($mbmb, $pcpc,"code"=>$r['code']);
			}
		}
		return $a;
	}
}
if (!function_exists("calc_no_deposit_bonus")) {
	function calc_no_deposit_bonus() {
		global $sitelan; global $exclusive_bonus_allowed; global $sitetype;
		if (!$sitetype) $sitetype="casino";
		$idname=array("casino"=>"casino_id","poker"=>"poker_id","sport"=>"bm_id");
		$bonuses_new_name=array("casino"=>"bonuses_new","poker"=>"bonuses_new_pokerrooms","sport"=>"bonuses_new_bookmakers");

		$the_history=array(); $a=array();
		if (!$exclusive_bonus_allowed) $addexc=" AND IFNULL(is_exclusive_bonus,0) = 0";
			$query="select * from {$bonuses_new_name[$sitetype]} where 
			   onlyfornotfor ='onlyfor $sitelan'
			   $addexc
			   AND {$idname[$sitetype]} IS NOT NULL 
			   and (bonus_type='Nodeposit' OR bonus_type='Freespins')
			   ORDER BY {$idname[$sitetype]}, max_bonus DESC";
		$result=mnq($query);
		while ($r=mfa($result)) {
			if (!in_array($r[$idname[$sitetype]], $the_history)) {
				$the_history[]=$r[$idname[$sitetype]];
				if ($r['bonus_type']=="Freespins")
					$a[$r[$idname[$sitetype]]]=array($r['max_bonus']." Free spins","code"=>$r['code']);
				else
					$a[$r[$idname[$sitetype]]]=array($r['max_bonus'],"code"=>$r['code']);
			}
		}
		
		return $a;
	}
}
if (!function_exists("getbonussumnew")) {
	function getbonussumnew($r) {
		global $sitelan; global $sitetype; global $freebetforsports;
		if (!$sitetype) $sitetype="casino";
		$idname=array("casino"=>"casino_id","poker"=>"poker_id","sport"=>"bm_id");
		$bonus_table_name=array("casino"=>"bonuses_general","poker"=>"bonuses_general_pokerrooms","sport"=>"bonuses_general_bookmakers");
		$query="select * from {$bonus_table_name[$sitetype]} where {$idname[$sitetype]}='{$r[$idname[$sitetype]]}' and NULLIF(bonus_sum,'') IS NOT NULL and lan = 'onlyfor $sitelan'";
		$result=mnq($query);
		if ($rr=mfa($result)) {
			$freebetforsports=$rr['freebetforsports'];
			return $rr['bonus_sum'];
		} 
	}
}
if (!function_exists("currencychange")) {
	function currencychange($c, $addspace="", $standard_currency_code=false, $onlyshortencurrency=false) {
		global $ipcountry; global $sitelan; global $dict;
		preg_match("@(.*)(<span>.*</span>)@",$c,$matchesx);
		if ($matchesx) {
			$c=$matchesx[1];
			$extra=$matchesx[2];	
		}
		if (!$dict['free spins']) $dict['free spins']="free spins";
		if (stripos($c,$dict['free spins'])!==false) return $c.$extra;
		
		if ($onlyshortencurrency) {
			$final=$c;
		} else {
	
			$a=array(); 
			
			$cs=explode(",",$c);
			foreach ($cs as $css) {
				$a[substr($css,-3)]=$css;
			}
			switch($ipcountry) {
				case "fr":
					$final=$a['EUR'] OR $final=$a['USD'] OR $final=$a['GBP'] OR  $final=current($a);
					break;
				case "us":
					$final=$a['USD'] OR $final=$a['GBP'] OR $final=$a['EUR'] OR  $final=current($a);
					break;
				case "gb":
					$final=$a['GBP'] OR $final=$a['EUR'] OR $final=$a['USD'] OR  $final=current($a);
					break;
				case "ca":
					$final=$a['CAD'] OR $final=$a['USD'] OR $final=$a['EUR'] OR  $final=current($a);
					break;
				case "ch":
					$final=$a['CHF'] OR $final=$a['EUR'] OR $final=$a['USD'] OR  $final=current($a);
					break;
				case "se":
					$final=$a['SEK'] OR $final=$a['EUR'] OR $final=$a['USD'] OR  $final=current($a);
					break;
				case "no":
					$final=$a['NOK'] OR $final=$a['EUR'] OR $final=$a['USD'] OR  $final=current($a);
					break;
				case "za":
					$final=$a['ZAR'] OR $final=$a['USD'] OR $final=$a['EUR'] OR  $final=current($a);
					break;
				case "br":
					$final=$a['BRL'] OR $final=$a['USD'] OR $final=$a['EUR'] OR  $final=current($a);
					break;
				case "mx":
					$final=$a['MXN'] OR $final=$a['USD'] OR $final=$a['EUR'] OR  $final=current($a);
					break;
				case "ar":
					$final=$a['ARS'] OR $final=$a['USD'] OR $final=$a['EUR'] OR  $final=current($a);
					break;
				case "au":
					$final=$a['AUD'] OR $final=$a['USD'] OR $final=$a['EUR'] OR  $final=current($a);
					break;
				case "es":
				case "de":
				case "at":
				case "fi":
				case "be":
				case "pt":
				case "ro":
				case "it":
					$final=$a['EUR'] OR $final=$a['USD'] OR $final=$a['GBP'] OR $final=current($a);
					break;
				default:
					if ($sitelan=="en") $final=$a['USD'] OR $final=$a['GBP'] OR $final=$a['EUR'] OR  $final=current($a);
					elseif ($sitelan=="fr") $final=$a['EUR'] OR $final=$a['USD'] OR $final=$a['GBP'] OR  $final=current($a);
					elseif ($sitelan=="de") $final=$a['EUR'] OR $final=$a['USD'] OR $final=$a['GBP'] OR  $final=current($a);
					elseif ($sitelan=="es") $final=$a['EUR'] OR $final=$a['USD'] OR $final=$a['GBP'] OR  $final=current($a);
					elseif ($sitelan=="it") $final=$a['EUR'] OR $final=$a['USD'] OR $final=$a['GBP'] OR  $final=current($a);
					elseif ($sitelan=="sv") $final=$a['SEK'] OR $final=$a['EUR'] OR $final=$a['USD'] OR  $final=current($a);
					elseif ($sitelan=="no") $final=$a['NOK'] OR $final=$a['EUR'] OR $final=$a['USD'] OR  $final=current($a);
					else $final=$a['EUR'] OR $final=$a['USD'] OR $final=$a['GBP'] OR  $final=current($a);
			}
			
			if ($standard_currency_code) return $final.$extra;
			
		}
		
		
		switch($sitelan) {
			case "fr": 
			case "de":
			case "it":
				if (in_array($ipcountry,array("ch"))) {
					$currencyposition="before";
				} else {
					$currencyposition="after";
				}
				break;
			case "en": 
				$currencyposition="before"; break;
			case "es": 
				if (in_array($ipcountry,array("es"))) {
					$currencyposition="after";
				} else {
					$currencyposition="before";
				}
				break;
			default: $currencyposition="after";
		}
		
		if ((float)$final==0) {
			if ($extra) return $extra; else return "&ndash;";
		}
	
		preg_match("@([0-9\.]+)(...)@",$final,$matches);
		if ($matches) {
			if ($addspace) {
				if ($currencyposition=="after") $final = $matches[1] . " " . $matches[2];
				else $final = $matches[2] . " " . $matches[1];
			} else {
				if ($currencyposition=="after") $final = $matches[1] . $matches[2];
				else $final = $matches[2] . $matches[1];
			}
			
			
		} else {
			if ($extra) return $extra; else return "&ndash;";
		}
	
		if (in_array($ipcountry,array("mx","ar")))
			$final=strtr($final,array("USD"=>"US$"));
		
		$final = strtr($final,array("USD"=>"$","CAD"=>"$","EUR"=>"€","GBP"=>"£","SEK"=>"kr","NOK"=>"kr","ZAR"=>"R","BRL"=>"R$","MXN"=>"$","ARS"=>"$","AUD"=>"$"));
		
		
		return $final.$extra;
	}
}
if (!function_exists("casinoswithlogos")) {
	function casinoswithlogos($field) {
		if (!file_exists($_SERVER['DOCUMENT_ROOT']."/$field.txt")) update_md5($field);
		$s=file_get_contents($_SERVER['DOCUMENT_ROOT']."/$field.txt");
		preg_match_all("@(.+)\.(gif|png|jpe*g) \S{32}\r$@m",$s,$matches);
		return $matches[1];
	}
}
if (!function_exists("getmenumobile")) {
	function getmenumobile() {
		global $maintable; global $site_table_criteria; global $sitedata;
		$query="select * from $maintable where (parent_link='/' or page_url='/') AND theorder>0$site_table_criteria order by if(page_url='/',0,1), theorder";
		$result=mnq($query);
		while ($r=mfa($result)) {
			$t=$r['menutitle'] OR $t=$r['maintitle'];
			switch ($r['type']) {
				default:
					$s.=<<<A
<option value="{$sitedata['suffix']}{$r['page_url']}">$t</option>
A;
			}
			
		}
		return $s;
	}
}


if (!function_exists("addparentlink")) {
	function addparentlink($s) { 
		global $site; 
		
		if (!$site['parent_link']) return strtr($s,array("{"=>"","}"=>""));
		
		$s=preg_replace("@\{(.+)\}@U", "<a href='{$GLOBALS['sitedata']['suffix']}{$site['parent_link']}'>$1</a>", $s);
		return $s;
	
	}
}
function univ_breadcrumb($metitle="",$meparentlink="") {
	global $site; global $sitedata; global $sitelan; global $subdomain; global $master_short; global $pagename;
	global $site_table_criteria;
	
	if (!$site_table_criteria) $site_table_criteria=site_table_criteria();

	$e=explode(",",$sitedata['db_tables']);
	$cururl=$site['page_url'];
	if ($master_short=="tcc") {
		$e=array("tcc_site");
	}
	if ($e[0]=="") return;
	
    $query="select menutitle, maintitle, page_url, parent_link, type from {$e[0]} where theorder>0$site_table_criteria order by id DESC";
    $result=mnq($query);
	if (!$result) return;
	
    while ($r=mfs($result)) {
    	$tt=$r['menutitle'] OR $tt=$r['maintitle'];
		switch ($master_short) {
			case "pro":
				if ($r['type']=="news") $r['parent_link']="/actualite/";
				break;	
		}
    	$a[$r['page_url']]=array($tt, $r['parent_link']);
    }

	if ($master_short=="tcc" and $pagename=="review") {
		$query="select menutitle, maintitle, page_url, parent_link from casinos_plus where theorder>0$site_table_criteria order by id DESC";
		$result=mnq($query);
		
		while ($r=mfs($result)) {
			$tt=$r['menutitle'] OR $tt=$r['maintitle'];
			$a[$r['page_url']]=array($tt, $r['parent_link']);
		}
	}

	if (!$a[$cururl] and $metitle and $meparentlink) $a[$cururl]=array($metitle,$meparentlink);
    $u=$a[$cururl];
    $k="";
    while (1) {
		if (!$k) $hr=""; else $hr=" href='{$GLOBALS['sitedata']['suffix']}$k'";
    	$s[]="<a$hr>{$u[0]}</a>";
        if ($k=="/") break;
        $k=$u[1];
    	$u=$a[$u[1]];
        if (!$u) break;
    }
    $s=array_reverse($s);

	if ($s)	return "<div id='breadcrumb'>".implode(" &gt; ",$s)."</div>";
}
if (!function_exists("addsisterlinks")) {
	function addsisterlinks() {
		global $site; global $maintable; global $sitelan; global $subdomain; global $master_short; global $pagename;
		global $sitedata; global $valid_casinos_bookmakers_pokerrooms; global $site_table_criteria; 
		if (!$site_table_criteria) $site_table_criteria=site_table_criteria();

		if (!$site['parent_link']) return;
		if (array_key_exists('no_univ_breadcrumb',$sitedata) AND !$sitedata['no_univ_breadcrumb']) {
			$metitle=$site['menutitle'] OR $metitle=$site['maintitle'];
			$meparentlink=$site['parent_link'];
			$univ_breadcrumb=univ_breadcrumb($metitle,$meparentlink);
		}

		if (!$valid_casinos_bookmakers_pokerrooms) 
			$valid_casinos_bookmakers_pokerrooms=valid_casinos_bookmakers_pokerrooms();
					
		if ($site['bottom_text']) {
			if ($site['bottom_text']=="none") $site['bottom_text']="";
			if ($master_short=="tcc" and $pagename=="review") $tbb="casinos_plus"; else $tbb=$maintable;
			$query="select * from $tbb where id != {$site['id']} and parent_link='{$site['parent_link']}' and theorder>0$site_table_criteria{$valid_casinos_bookmakers_pokerrooms[1]} order by theorder";
			$result=mnq($query);
			if ($result) {
				while ($r=mfs($result)) {
					$t=$r['menutitle'] OR $t=$r['maintitle'];
					$s.="<li><a href='{$GLOBALS['sitedata']['suffix']}{$r['page_url']}'>$t</a></li>";
				} 
				$site['bottom_text']=formattext(wholeb2h3($site['bottom_text']));
			}
		}
		$p=$univ_breadcrumb;
		if ($s) $p.="<div id='sistersw'>" . $site['bottom_text'] . "<div style='clear:both; margin:20px 0;'></div><ul>$s</ul></div>";
		return $p;
	}
}
if (!function_exists("wholeb2h3")) {
	function wholeb2h3($s) {
		$s=preg_replace("@</b>(\h*)<b>@i", "$1", $s);
		return preg_replace("@^\h*<b>(((?!<b>).)+)</b>\h*(\r|$)@im", "<h3>$1</h3>\r", $s);
	}
}
function newsindex_url() {
	global $maintable; global $table_critera; global $sitelan; global $subdomain;
	if ($table_critera) {
		$others=" AND ".strtr($table_critera,array("*lan*"=>$sitelan,"*subdomain*"=>$subdomain));
	}
	$query="select * from $maintable where (type='newsindex' or type_admin='newsindex') and theorder>0$others";
	$result=mnq($query);
	if ($r=mfs($result)) return $r['page_url']; else return "";
}
if (!function_exists("menunew")) {
	function menunew($parent,$excludedtypes=array()) {
		global $menus; global $maintable; global $table_critera; global $sitelan; global $subdomain;
		global $site_table_criteria; global $sitedata; global $valid_casinos_bookmakers_pokerrooms;
		
		if (!$valid_casinos_bookmakers_pokerrooms) 
			$valid_casinos_bookmakers_pokerrooms=valid_casinos_bookmakers_pokerrooms();

		if ($table_critera) {
			$others=" AND ".strtr($table_critera,array("*lan*"=>$sitelan,"*subdomain*"=>$subdomain));
		}
		if ($parent=="/") 
			if (function_exists(partners)) $partners=partners();
	
		if (!$menus) {
			if ($excludedtypes) {
				$excludedtypesstring=implode(",",$excludedtypes);
				$excludedtypesstring=" and !find_in_set(IFNULL(type,''),'$excludedtypesstring')";
			}
			$query="select * from $maintable where theorder>0$excludedtypesstring and (NULLIF(parent_link,'') IS NOT NULL OR page_url='/' OR type='news')$others$site_table_criteria order by IF(type='news',9999,theorder), id DESC";
			$result=mnq($query);
			while ($r=mfa($result)) {
				if ($r['type']=="review" and !in_array($r[$valid_casinos_bookmakers_pokerrooms[2]],$valid_casinos_bookmakers_pokerrooms[0])) continue;
				if ($r['type']=="news" and !$r['parent_link']) {
					global $newsindex_url;
					if (!isset($newsindex_url)) $newsindex_url=newsindex_url();
					if ($newsindex_url) {
						$r['parent_link']=$newsindex_url;
					} else $r['parent_link']="/";
				}
				$t="";
				$t=$r['menutitle'] or $t=$r['maintitle'];
				$menus[$r['parent_link']][]=array($r['page_url'],$t);
			}
		}
		if (is_array($menus[$parent])) {
			foreach ($menus[$parent] as $m) {
				$s.="<li><a href='{$sitedata['suffix']}{$m[0]}'>{$m[1]}</a>".menunew($m[0]) ."</li>";
			}
		} elseif ($parent=="/") {
			$query="select menutitle, page_url, maintitle from $maintable where page_url!='/sitemap.html' and theorder>0$others$site_table_criteria order by theorder";
			$result=mnq($query);
			while ($r=mfa($result)) {
				$t="";
				$t=$r['menutitle'] or $t=$r['maintitle'];
				$s.="<li><a href='{$sitedata['suffix']}{$r['page_url']}'>$t</a></li>";
			}    

			return "<ul><li><a class='nohover'>Menu</a><ul>$s</ul></li>$partners</ul>";
		}

		if ($s) {
			if ($parent=="/") 
				return "<ul><li><a href='{$sitedata['suffix']}/'>{$menus[""][0][1]}</a></li>$s$partners</ul>";
			else
				return "<ul>$s</ul>";
		}
	}
	}
if (!function_exists("partners")) {
	function partners() {
		global $sitelan; global $maintable; global $sitedata; global $site;
		$sitelanshort=substr($sitelan,0,2);
		$localurl=$sitedata['short'];
		if ($_SERVER['REMOTE_ADDR']=="127.0.0.1") {
			$query="select * from manage_websites where NULLIF(short,'') IS NOT NULL AND NULLIF(url,'') IS NOT NULL";
			$result=mnq($query);
			while ($r=mfa($result)) {
				$long2short[$r['url']]=$r['short'];
			}    
			$query="select * from partners where NULLIF(siteid,'') IS NULL AND NULLIF(longurl,'') IS NOT NULL";
			$result=mnq($query);
			while ($r=mfa($result)) {
				$r['longurl']=trim(strtr($r['longurl'], array("http://"=>"")));
				preg_match("@^[^\/]+@", $r['longurl'], $matches);
				$url= trim($matches[0]);
				$q="update partners set siteid='{$long2short[$url]}', longurl='$url' where id={$r['id']}";
				mnq($q);
			}
		}
		
		$query="select * from partners where siteid='$localurl'";
		$result=mnq($query);
		if ($result) {
			if ($r=mfa($result)) {    
				if (!$r['homepageonly'] OR $site['page_url']=="/") {
					preg_match_all("@.+@", $r['partners'], $mmm);
					
					foreach ($mmm[0] as $aa) {
						$aa=trim($aa);
						preg_match("@^\s*([^\s]+)(.*)@", $aa, $matches);
						if ($matches) {
							$l=trim($matches[1]);
							if (substr($l, 0, 4)!="http") $l="http://$l";
							if (is_https($l)) $l=preg_replace("@^http\://@","https://",$l);
							else $l=preg_replace("@^https\://@","http://",$l);
							
							$k=trim(strtr($matches[2], array("anchor :"=>"","anchor:"=>"","anchor"=>"")));
							if (!$k) {
								$k=preg_replace("@^https?\://@i","",$l);
								$k=preg_replace("@^www\.@i","",$k);
								$k=preg_replace("@/$@","",$k);
								
							}
							$s.="<li><a href='$l'>$k</a></li>";
						}
					}
					$wordpartner=array("fi"=>"Kaverit","de"=>"Freunde","fr"=>"Amis","sv"=>"Vänner","en"=>"Friends","es"=>"Amigos");
					if ($s) return "<li id='partners'><a>{$wordpartner[$sitelanshort]}</a><ul>$s</ul></li>";
				}
			}
		}
	}
}
function image_in_box($filename, $width, $height, $proportional, $allowfill=1, $fillcolor="") {
	if (!$width or !$height) return;
	$styles=array();
	if (!preg_match("@^".preg_quote($_SERVER['DOCUMENT_ROOT'])."@",$filename)) {
		if (substr($filename,0,1)!="/") $filename="/".$filename;
		$filenamefullpath=$_SERVER['DOCUMENT_ROOT'].$filename;
	} else $filenamefullpath=$filename;
	$g=@getimagesize($filenamefullpath);
	if (!$g) return;
	if (!$proportional) {
		$styles['width']=$width;
		$styles['height']=$height;
	} else {
		$oriwidth=$g[0];
		$oriheight=$g[1];
		if ($allowfill) {
			if ($width/$height>$oriwidth/$oriheight) {
				$styles['height']=$height;
				$styles['width']=(int)($oriwidth*$height/$oriheight);
				$styles['margin-left']=($width-$styles['width'])/2;
				$styles['margin-right']=($width-$styles['width'])/2;
			} else {
				$styles['width']=$width;
				$styles['height']=(int)($oriheight*$width/$oriwidth);
				$styles['margin-top']=($height-$styles['height'])/2;
				$styles['margin-bottom']=($height-$styles['height'])/2;
			}
		} else {
			$fillcolor="";
			if ($width/$height>$oriwidth/$oriheight) {
				$styles['width']=$width;
				$styles['height']=(int)($oriheight*$width/$oriwidth);

			} else {
				$styles['height']=$height;
				$styles['width']=(int)($oriwidth*$height/$oriheight);
				$styles['margin-left']=($width-$styles['width'])/2;
			}
		
		}
	}

	if (function_exists("optimize_image")) {
		$filename=optimize_image($filename,$styles['width'],$styles['height']);
	}
	
	
	$final=array();
	foreach ($styles as $key=>$style) {
		$p="$key:$style";
		$p.="px";
		$final[]=$p;
	}
	$final=implode(";",$final);
	if ($fillcolor) $addbg="background-color:$fillcolor;";
	$s="<span class='imgspan' style='display:inline-block;overflow:hidden;width:{$width}px;height:{$height}px;$addbg'><img src='$filename' style='$final' /></span>";	
	return $s;
}
function adjust_main_image($maxwidth="") {
	global $site; global $sitedata;
	if (!$site['image']) return;
	if (!$maxwidth) {
		switch ($sitedata['using_template']) {
			case "1":
				if (!$sitedata['menu_width']) $sitedata['menu_width']=157;
				$sitedata['main_part_width']=960-$sitedata['menu_width'];
				$maxwidth=$sitedata['main_part_width']-40;
				break;
			case "2":
				if (!$sitedata['menu_width']) $sitedata['menu_width']=220;
				$sitedata['main_part_width']=868-$sitedata['menu_width'];
				$maxwidth=$sitedata['main_part_width']-44;
				break;
			default:
				$maxwidth=765;
		}
	}

	if (preg_match("@^~~temp~~(\d+)~(.+)@",$site['image'],$mch)) {
		if (strpos($_SERVER['HTTP_HOST'],".")===false) {
			$iname="http://bcel/img/drafts/{$mch[1]}/{$mch[2]}";
			$inamefull=$iname;
		} else {
			$iname="http://www.bonus-casino-en-ligne.info/img/drafts/{$mch[1]}/{$mch[2]}";
			$inamefull=$iname;
		}
	} else {
		if (strpos($site['image'],"/")===false) $iname="/img/site/{$site['image']}"; else $iname=$site['image'];
		if (substr($iname,0,1)!="/") $iname="/".$iname;
		$inamefull=$_SERVER['DOCUMENT_ROOT'].$iname;
	}
	$style="float:left; margin:5px 10px 0 0; border:1px #eee solid;";
	$site['picmain_maxwidth']=$maxwidth;
	$imgsize=@my_getimagesize($inamefull);
	if (!$imgsize) return;
	$site['picmain_width_ori']=$imgsize[0];
	$site['picmain_height_ori']=$imgsize[1];
	if ($site['picmain_width'] AND !$site['picmain_height']) {
			$site['picmain_height']=$site['picmain_height_ori']*$site['picmain_width']/$site['picmain_width_ori'];
	} elseif ($site['picmain_height'] AND !$site['picmain_width']) {
			$site['picmain_width']=$site['picmain_width_ori']*$site['picmain_height']/$site['picmain_height_ori'];
	} elseif (!$site['picmain_width'] AND !$site['picmain_height']) {
		$site['picmain_width']=$site['picmain_width_ori'];
		$site['picmain_height']=$site['picmain_height_ori'];
	}
	if ($site['picmain_width']>$site['picmain_maxwidth']*0.85) {
		$site['picmain_height']=$site['picmain_height']*$site['picmain_maxwidth']/$site['picmain_width'];
		$site['picmain_width']=$site['picmain_maxwidth'];
		$style="float:none; margin:5px 0 10px 0;";
		$dontfloat=true;
	}
	$site['picmain_width']=(int)$site['picmain_width'];
	$site['picmain_height']=(int)$site['picmain_height'];
	if (function_exists("optimize_image")) {
		$iname=optimize_image($iname,$site['picmain_width'],$site['picmain_height'],$site['picmain_width_ori'],$site['picmain_height_ori']);
	}
	$style.=" width:{$site['picmain_width']}px; height:{$site['picmain_height']}px;";
	
	$site['image_alt_text']=htmlentities($site['image_alt_text'],ENT_COMPAT,"UTF-8");
	$image= "<img src='$iname' alt=\"{$site['image_alt_text']}\" style='$style'>";
	
	if ($site['image_link']) {
		$image="<a href='{$site['image_link']}'>$image</a>";
		$image=strtr($image, array("href='www"=>"href='http://www"));
	}
	if ($site['under_image']) {
		$underi=preg_split("@(<a .+</a>)@Ui",$site['under_image'], -1, PREG_SPLIT_DELIM_CAPTURE);
		$ni=-2;
		while ($ni<count($underi)-2) {
			$ni=$ni+2;
		$underi[$ni]=preg_replace("@((?:http\:\/\/|www).\S+)@s", "<a href='$1'>$0</a>", $underi[$ni]);
		}
		$site['under_image']=implode("",$underi);
		
		$site['under_image']=strtr($site['under_image'], array("href='www"=>"href='http://www"));
		if ($dontfloat) {
			$image=preg_replace("@float\:none\; margin\:5px 0 10px 0\;@", "float:none; margin:5px 0 0 0;", $image);
			$widthnofloat=(int)$site['picmain_width']-2;
			$image.="<span id='credit' style='clear:both;width:{$widthnofloat}px; font-style:italic; display:block; float:none; text-align:center; margin-bottom:5px; padding:5px 0;'>{$site['under_image']}</span>";
		} else {
			$image.="<span id='credit' style='clear:both;width:{$site['picmain_width']}px; font-style:italic; display:block; float:left; margin-right:10px; text-align:center; margin-bottom:5px; padding:5px 0;'>{$site['under_image']}</span>";
		}
	}
	if ($dontfloat) return "<center>$image</center>"; else return $image;		
}


function update_fillin ($tablename) {
	return;
}

$admindiv.="<!-- page ready -->";



// functions end *********************


	if ($_POST['scode']=="mJii8jed_edKKio130830NHgge^^") { //this code for posting keywords
		$keywords=stripslashes($_POST['keywords']);
		echo modifykeywords();
		exit;	
	}


// add admin part in javascript to site

function adminagain() {
	global $univ_live_admin;
	echo $univ_live_admin;
}

function file_exists_after_converting($targetimgname, $targetwidth="", $targetheight=0, $imgsource="", $colordepth=0, $leastheight=0, $overlay=0, $forceupdate=0, $background_r=0, $background_g=0, $background_b=0) {
//updated 20171124

	if ($_GET['forceupdate']) $forceupdate=1;

	preg_match("#(\.(jpe?g|png|gif))$#i", $targetimgname, $matches);
	$targetfiletype=$matches[1];
	if (!$targetfiletype) return;

	$targetimgname=preg_replace("#(https?\://{$_SERVER['HTTP_HOST']})(.+)#iU", "$2", $targetimgname);
	if ((substr($targetimgname, 0, 1)=="/")) $targetimgname=$_SERVER['DOCUMENT_ROOT'].$targetimgname;
	
    if ($forceupdate==0) {
		if (file_exists_case_sensitive($targetimgname)) return true;
    }
	$image = open_image($targetimgname, $imgsource, $originalsize); //xxxx.gif
	if ($image === false) { return; }
	
	$w = imagesx($image);
	$h = imagesy($image);
	if (!$targetwidth) $targetwidth=$w;
    if (!$targetheight) {
    	$targetheight=(int) ($targetwidth * $h / $w);
        if ($targetheight<$leastheight) $targetheight=$leastheight;
    }
	$im2 = ImageCreateTrueColor($targetwidth, $targetheight);
    
    if ($targetfiletype==".gif" or $targetfiletype==".png") { // keep alpha
        imagecolortransparent($im2, imagecolorallocatealpha($im2, 0, 0, 0, 127));
        imagealphablending($im2, false);
        imagesavealpha($im2, true);    
    } else { // create a background
        $background = imagecolorallocate($im2, $background_r, $background_g, $background_b);
        imagefill($im2, 0, 0, $background);
    }
    
	imagecopyResampled ($im2, $image, 0, 0, 0, 0, $targetwidth, $targetheight, $w, $h);
    
    if ($overlay) {
    	$overlayfile = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'].$overlay);
        imagecopy($im2,$overlayfile,0,0,0,0,$targetwidth, $targetheight);
    }

    $folder_of_target=preg_replace("@/[^/]+$@","",$targetimgname);
    if (!file_exists($folder_of_target)) {
        if (!mkdir($folder_of_target,0775,true)) return false;
    }
            
	switch ($targetfiletype) {
		case ".gif":
			imagegif($im2, $targetimgname);
			break;
		case ".jpg":
        case ".jpeg":
			imagejpeg($im2, $targetimgname);
			break;
		case ".png":
			if ($colordepth) imageTrueColorToPalette($im2, true, $colordepth);
			imagepng($im2, $targetimgname, 1);
			break;
	}
	
	imagedestroy($im2); 
	imagedestroy($image);
	return true;
}
if (!function_exists("open_image")) {
    function open_image ($file, $path="") { //20170713
        $im=false;
        preg_match("#([^\/]+)\.(jpe?g|png|gif)$#i", $file, $matches); 
        $f=$matches[1];
		$oriext=$matches[2];
        if (!$path) {
            preg_match("#(.*/)#i", $file, $matches);
            $path=$matches[1];
        }
        if (substr($path, 0, 1)=="/") $path=$_SERVER['DOCUMENT_ROOT'].$path;
		
        if (substr($path,-1)=="/") 
			$fs=array("$path$f.$oriext");
        elseif (preg_match("@/[^\./]+$@",$path)) 
			$fs=array($path, "$path/$f.$oriext");
        else $fs=array($path);
		
        foreach ($fs as $f) {
            if ($itype=@my_exif_imagetype($f)) {
                return loadimg($f);
                break;	
            }
        }
        return false;
    }
}
function loadimg_by_ftp($url) {
	$conn_id = ftp_connect("141.101.62.113");
	$login_result = ftp_login($conn_id, "images", "wPf%WPZAJn");
	$file="/". preg_replace("@^http\://www.bonus-casino-en-ligne.info/@i","",$url);
	ob_start();
	$result = ftp_get($conn_id, "php://output", $file, FTP_BINARY);
	ftp_close($conn_id);
	$rawdata = ob_get_contents();
	ob_end_clean();
	if ($rawdata) {
	    $image = @imagecreatefromstring($rawdata);
		return $image;
	}
}
function loadimg($url) {
	if (preg_match("@^https?\://@i",$url)) {
		if (preg_match("@^http\://www.bonus-casino-en-ligne.info/@i",$url)) return loadimg_by_ftp($url);
        $ch = curl_init();
    
        curl_setopt ($ch, CURLOPT_URL, $url);    
        curl_setopt ($ch, CURLOPT_BINARYTRANSFER, true);  
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);  
        curl_setopt ($ch, CURLOPT_HEADER, false);  
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 0); 
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        $rawdata = curl_exec($ch);
        curl_close($ch);
    } else $rawdata=file_get_contents($url);
    if ($rawdata) $image = @imagecreatefromstring($rawdata);
    return $image;
}
if (!function_exists("file_exists_case_sensitive")) {
    function file_exists_case_sensitive($f) {
        if (!$f) return false;
        
        if ($_SERVER['SystemRoot']!="C:\Windows") return file_exists($f);
    
        global $filesinfolder;
    
        if (substr($f,1,1)==":") $isabsolutepath=true;
        
        if (!$isabsolutepath) $f=$_SERVER ['DOCUMENT_ROOT']."/$f";
    
        preg_match("@(.+/)(.+)@", $f, $matches);
        
        $dir.=$matches[1];	
        $f=$matches[2];

        if (!$filesinfolder[$dir]) $filesinfolder[$dir] = @scandir($dir); 
        
        if (!$filesinfolder[$dir]) return false;
    
        if (in_array($f, $filesinfolder[$dir])) return true; else return false;
    }
}
function optimize_image($filenames,$newwidth=0,$newheight=0,$oriwidth=0,$oriheight=0,$special="") {
	//## filename format example: img/site/xxxx.jpg, if no dir path, path will be img/site/
    if ($GLOBALS['previewmode']) return $filenames;
//	if (strpos($_SERVER['HTTP_HOST'],".")===false) return $filenames;

	if ($newwidth==0 and $newheight==0) return $filenames;

	$final=array();
	if (!is_array($filenames)) {
    	$filenames=array($filenames);
    } else $sourceisarray=true;

	foreach ($filenames as $filename) {
    	$addslash="";
        $hasfoldername=false;
        
        preg_match("@(.*)([^/]+)\.(jpe*g|png|gif)$@iU",$filename,$matches);
        if (!$matches) {
        	$final[]= $filename;
            continue;
        }
    
        if ($matches[1]) {
            $filenamewithpath=$filename;
            $filenamewithoutpath=$matches[2].".".$matches[3];
            $folder=$matches[1];
            if (substr($folder,0,1)=="/") {
                $folder=substr($folder,1);
                $addslash="/";
            }
            $hasfoldername=true;
        } else {
            $filenamewithpath="img/site/".$filename;
            $filenamewithoutpath=$filename;
            $folder="img/site/";
        }
        
        $newwidth=(int)$newwidth;
        $newheight=(int)$newheight;

        if (!$oriwidth OR !$oriwidth) {
            $imgsize=@getimagesize($_SERVER['DOCUMENT_ROOT']."/".$folder. $filenamewithoutpath);
            if (!$imgsize) {
            	$final[]= $filename;
                continue;
            }
            $oriwidth=$imgsize[0];
            $oriheight=$imgsize[1];
        }
    
		if ($newwidth>0 and $newheight>0 and $special=="proportional") {
        	$oldproportion=$oriwidth/$oriheight;
            if ($newwidth/$newheight<$oldproportion) {
            	$newheight=(int)($oriheight*$newwidth/$oriwidth);
            } else {
            	$newwidth=(int)($oriwidth*$newheight/$oriheight);
            }
        }

		if ($newwidth==0) $newwidth=(int)($newheight*$oriwidth/$oriheight);
        if ($newheight==0) $newheight=(int)($newwidth*$oriheight/$oriwidth);

        if ($newwidth>=$oriwidth and $newheight>=$oriheight) {
        	$final[]= $filename;
            continue;
        }
        $newfilename= "~{$newwidth}/$newheight/".$filenamewithoutpath;
        if (file_exists_after_converting("{$folder}$newfilename",$newwidth,$newheight,"{$folder}$filenamewithoutpath")) {
            if ($hasfoldername) {
                $final[]= "$addslash{$folder}$newfilename";
                continue;
            }
            else {
                $final[]= "$newfilename";
                continue;
            }
        
        }
        else {
        	$final[]= $filename;    	
        }
    }

	if ($sourceisarray) return $final; else return $final[0];
    
}
function oddsdata($matches) {
	global $sitedata;	
    
    if ($sitedata['using_template']=="cfo") {
		$visitlinkformat="/visit/";
		$xpp="php";
	} else {
		$visitlinkformat="/go/";
		$xpp="html";
	}
	
	$query="select * from manage_sports_betting where id={$matches[1]}";
    $result=mnq($query);
    if ($result) {
    	if ($r=mfs($result)) {
            $n=array();
        	$v=array();
            $cols=array();
        	preg_match_all("@^(.+)\:(.*)\r*$@Um",$r['odds'],$matches);
            foreach ($matches[1] as $key=>$o) {
            	$n[$key]=$o;
            	$j=explode("|",$matches[2][$key]);
                foreach ($j as $jj) {
                	preg_match("@(.+) ([0-9\.]+)$@",$jj,$mat2);
                    if ($mat2) {
                		if (!in_array($mat2[1],$cols)) {
                        	$cols[]=$mat2[1];
                        }
                        $v[$key][$mat2[1]]=$mat2[2];
                        
                	}
                }
                
            }
            foreach ($v as $ind=>$k) {
            	$name=id2name("bookmakers",$n[$ind]);
            	$s.="<tr><td><a target='_blank' href='$visitlinkformat{$n[$ind]}.$xpp'>$name</a></td>";
                foreach ($cols as $col) {
                	$s.="<td>{$v[$ind][$col]}</td>";
                }
                
                $s.="</tr>";            
            }
            
            
        }
    }
    if ($s) {
    	foreach ($cols as $col) {
        	$row1.="<th>$col</th>";
        }
    	return "<table id='sportodds' cellpadding='0' cellspacing='0'><tr><th>Bookmaker</th>$row1</tr>$s</table>";
    }
}
if (!function_exists("formattext")) {
    function formattext($t, $addlink="", $selfurl="") {
    	global $using_border_box; global $imagepath; global $site_protocol;
        if (!$imagepath) $imagepath="/img/site/";
        $t=trim($t);
        if ($t=="") {return "";}

        if ($site_protocol=="https://") $t=preg_replace("@<iframe[^>]+src\s*\=\s*[\"|\']http\://[^>]+>\s*</iframe>@Uis","",$t);
        $t= adjust_https($t);
        $t= preg_replace_callback("@<(i|b|em|strong)>((?:(?!<\g1[> ]).)+)</\g1>@Uis","puttageachline",$t);
		$t= preg_replace_callback("@<table(\h(?:(?!<table).)+)*>.*</table>@Uisu","rearrangetable",$t);
    	$t= preg_replace("@<iframe.+>.*</iframe>@Uisu","<center>$0</center>",$t);
        $t= preg_replace_callback("@\*odds(\d+)\*@U","oddsdata",$t);
        if (preg_match_all("@(?:<p>)*\*img(\d+)\*(?:</p>)*@", $t, $matches)) {
            global $site;
            $imgs=array();
	preg_match_all("@^(\d+)\:([^\/]+)(?:\/([^\/]*)|)(?:\/([^\/]*)|)(?:\/([^\/]*)|)(?:\/(.*)(?:\{\}(.*)|)(?:\{\}(.*)|)|)(?:\r|)$@mU", $site['images'], $matches2);
            foreach ($matches2[1] as $key=>$m2) {
                $imgs[$m2]=array($matches2[2][$key],$matches2[3][$key],$matches2[4][$key],$matches2[5][$key],$matches2[6][$key],$matches2[7][$key],$matches2[8][$key]);
                if (!in_array($imgs[$m2][1],array("none","right"))) $imgs[$m2][1]="left";
                if ($imgs[$m2][2]) $imgs[$m2][2].="px"; else $imgs[$m2][2]="auto";
                if ($imgs[$m2][3]) $imgs[$m2][3].="px"; else $imgs[$m2][3]="auto";
            }
            if ($imgs) {
                foreach ($matches[0] as $key=> $m) {
                    $imgid=$matches[1][$key];
					if (!$imgs[$imgid][0]) {
	                    $t=strtr($t,array($m=>""));
                        continue;
                    }
                    if (preg_match("@^~~temp~~(\d+)~(.+)@",$imgs[$imgid][0],$mch)) {
                        if (strpos($_SERVER['HTTP_HOST'],".")===false)
                            $isrc="http://bcel/img/drafts/{$mch[1]}/{$mch[2]}";
                        else
                            $isrc="http://www.bonus-casino-en-ligne.info/img/drafts/{$mch[1]}/{$mch[2]}";
                        $fullsrc=$isrc;
                    } else {
                        $isrc="$imagepath{$imgs[$imgid][0]}";
                        $fullsrc=$_SERVER['DOCUMENT_ROOT'].$isrc;
                    }

                    if ($imgs[$imgid][2]=="auto") {
                        $imginfo=@getimagesize($fullsrc);
                        $imgs[$imgid][2]=$imginfo[0]."px";
                    }
                    if ($imgs[$imgid][4]) {
                        if (!$using_border_box) $txtwidth=(int)$imgs[$imgid][2] - 12;
                        else $txtwidth=(int)$imgs[$imgid][2];
                        $txt="<div style='max-width:{$txtwidth}px;'>{$imgs[$imgid][4]}</div>"; 
                    } else $txt="";
                    if (function_exists("optimize_image")) {
                        $isrc=optimize_image($isrc,$imgs[$imgid][2],$imgs[$imgid][3]);
                    }
                    $img="<div class='imgintxt float{$imgs[$imgid][1]}'>";
                    $imgs[$imgid][5]=htmlentities($imgs[$imgid][5],ENT_COMPAT,"UTF-8");
                    $imgcore="<img alt=\"{$imgs[$imgid][5]}\" style='width:{$imgs[$imgid][2]};height:{$imgs[$imgid][3]};' src='$isrc' />";
                    if ($imgs[$imgid][6]) $img.="<a href=\"{$imgs[$imgid][6]}\">$imgcore</a>";
                    else $img.=$imgcore;
                    $img.="$txt</div>";
                    $t=strtr($t,array($m=>$img));
                }
            }
        }
        if ($addlink) {	
            global $wordsarray;
            if (!$wordsarray) $addlink=0;
            if ($selfurl) {
                //delete self link:
                foreach ($wordsarray as $key => $vv) {
                    if (stripos($vv, $selfurl)!==false) $wordsarray[$key]=$key;
                }
            }
        }
        $myarray=explode(chr(13), $t);
        $t="";
        $nchange=array();
        foreach ($myarray as $m) {
            if ($m=trim($m)) {
			$m_plus=preg_replace("@<(a|b|em|strong|i)(\h(?:(?!/\g1>).)+)*>@iU","",$m);
			$m_plus=preg_replace("@</(a|b|em|strong|i)>@i","",$m_plus);
            if ((!(substr($m_plus, -1)==">" AND substr($m_plus, 0, 1)=="<")) AND !(substr($m_plus, 0, 3)=="<ul" OR substr($m_plus, 0, 3)=="<li")) {
            $m="<p>$m</p>";
            }
            $mlower=strtolower($m);
            if ($addlink AND (substr($mlower, 0, 2)=="<p" OR substr($mlower, 0, 3)=="<ul" OR substr($mlower, 0, 3)=="<li")) {
            $lastoff=0;
            $goods=0;
            $uuu=array();
            $toexit=false;
            while (!$toexit) {
                if (($lastoff=strpos($m, "<", $goods))===false) {
                    $lastoff=strlen($m); $toexit=true;}
                if ($goods != $lastoff) {$uuu[]=substr($m, $goods, $lastoff-$goods);}
                if (($goods=strpos($m, ">", $lastoff))!==false) {
                    $goods=$goods+1;
                    $uuu[]=substr($m, $lastoff, $goods-$lastoff);
                } else {break;}
            }
            $fs='';
            foreach ($uuu as &$u) {
                if (substr($u, 0, 1)!="<") {$fs.=strtr($u, $wordsarray);} else {$fs.=$u;}
            }
            $m=$fs;
            }
            $t=$t . $m;
            }
        }
    // remove redundant links
        $linkhistory=array();
        $theoffset=-1;
        while (strpos($t, "<a ", $theoffset+1) !== false) {
            $theoffset=strpos($t, "<a ", $theoffset+1);
            $endfirstpart=strpos($t, ">", $theoffset);
            $beginsecondpart=strpos($t, "</a>", $theoffset);
            $thelink=substr($t, $theoffset+2, $endfirstpart-3-$theoffset);
            if (strpos($thelink, "/casino/") !== false) {
            if (in_array($thelink, $linkhistory)) {
                $t=substr_replace($t, "", $beginsecondpart, 4);
                $t=substr_replace($t, "", $theoffset, $endfirstpart-$theoffset+1);
            } else {
            $linkhistory[]=$thelink;
            }}
        }
        return $t;
    }
    }
function puttageachline($matches) {
	$s=$matches[2];
    if (strpos($s,"\n")===false) return $matches[0];
	if (preg_match("@<(p|div)(>|\h)@iU",$s)) return $matches[0];
	$tag=$matches[1];
	$s=preg_replace("@\r*\n@","</$tag>\r\n<$tag>",$s);
	return "<$tag>$s</$tag>";
}
function rearrangetable($matches) {
    $s=$matches[0];
    $s=strtr($s,array(chr(13)=>"",chr(10)=>""));
    return $s;
}
function recursive_array_search($needle,$haystack) {
    foreach($haystack as $key=>$value) {
        $current_key=$key;
        if($needle===$value OR (is_array($value) && recursive_array_search($needle,$value) !== false)) {
            return $current_key;
        }
    }
    return false;
}
function newrating($id,$cat="review") {
	if (!$id) return;
	global $mastershort;
	$query="select * from {$mastershort}_rating_live where rating_id='$id' and cat='$cat'";
    $result=mnq($query);
    if (!$result) return;
    if (mnr($result)==0) {
    	$h=rand(3,15);
        $v=$h*rand(25,40)/10;
        $q="insert into {$mastershort}_rating_live (rating_id,howmanyvotes,votessum,cat) VALUES ('$id',$h,$v,'$cat')";
        mnq($q);
        $query="select * from {$mastershort}_rating_live where rating_id='$id' and cat='$cat'";
        $result=mnq($query);
    }
    $r=mfs($result);
    $average=round($r['votessum']/$r['howmanyvotes'],1);
    return array("howmanyvotes"=>$r['howmanyvotes'],"votessum"=>$r['votessum'],"average"=>$average);
}
function logobgcolor($theid) {
	global $sitetype;
	if (!$sitetype) $sitetype="casino";
	$idname=array("casino"=>"casino_id","poker"=>"poker_id","sport"=>"bm_id");
	$table=array("casino"=>"live_casinologo_dark_bgcolor","poker"=>"live_pokerlogo_dark_bgcolor","sport"=>"live_bmlogo_dark_bgcolor");
	$query="select * from {$table[$sitetype]} where {$idname[$sitetype]}='$theid'";
	$result=mnq($query);
	if ($r=mfs($result)) {
		$bgcolor=(string)$r['bgcolor'];
	}
	if (!$bgcolor) $bgcolor="000000";
	return "#$bgcolor";
}
function all_count_of_query($query) {
	$query=preg_replace("@ limit ((?! limit ).)+$@i", "", $query);
	$result=mnq($query);
	return mnr($result);
}
function get_a_free_game($id_unique) {
	global $freegamenewvar; global $sitelan; 
 	if (in_array($sitelan,array("en","fr","de","es","fi","sv","no"))) $lan=$sitelan; else $lan="en";
	$query="select * from {$freegamenewvar['datadb']} as t1 left join freegamenew_template as t2 on t1.template=t2.id left join {$freegamenewvar['maindb']} as t3 on t1.id_unique=t3.id_unique where t1.id_unique='$id_unique' AND t1.status>0 AND t2.$lan>0";   
    $result=mnq($query);
    if ($r=mfs($result)) {
    	unset ($r['id']);
    	return $r;
    } else return false;
}
function getfreegamenewquery($types="",$softwares="",$orderby="",$limit="",$exclusion="",$selectfileds="") {
	global $freegamenewvar; global $sitelan; global $master_short;
	if (in_array($sitelan,array("en","fr","de","es","fi","sv","no"))) $lan=$sitelan; else $lan="en";
    if ($selectfileds) {
    	$fls=explode(",",$selectfileds);
        foreach ($fls as &$flss) {
        	$flss="t1.".trim($flss);
        }
        $fls=implode(",",$fls);
	} else $fls="t1.*";

	$query="select $fls from {$freegamenewvar['maindb']} as t1 left join {$freegamenewvar['datadb']} as t2 on t1.id_unique=t2.id_unique left join freegamenew_template as t3 on t2.template=t3.id where t2.status>0 and t3.$lan>0";
	if ($types) {
		$finaltypes=array();
		if (!is_array($types)) $types=array($types);
		foreach ($types as $type) {
            switch ($type) {
                case "favorite":
                case "slot":
                    $finaltypes[]="video";
                    $finaltypes[]="video3d";
                    $finaltypes[]="classic";
                    break;
                case "3d":
                    $finaltypes[]="video3d";
                    break;
                case "videoslot":
                    $finaltypes[]="video";
                    break;
                case "classicslot":
                    $finaltypes[]="classic";
                    break;
                case "blackjack":
                    $finaltypes[]="blackjack";
                    $finaltypes[]="pontoon";
                    break;
                case "poker":
                    $finaltypes[]="poker";
                    $finaltypes[]="3cardpoker";
                    $finaltypes[]="caribbean";
                    $finaltypes[]="paigow";
                    break;
                default:
                    $finaltypes[]=$type;
            }
        }
        $finaltypes=array_unique($finaltypes);
		$query.=" AND find_in_set(t1.game_type,'".implode(",",$finaltypes)."')";
	}
	if ($softwares) {
		if (!is_array($softwares)) $softwares=array($softwares);
		$query.=" AND find_in_set(software_id,'".implode(",",$softwares)."')";
	} else $query.=$GLOBALS['freeres'];
    
	if ($master_short!="bcel") {
        if ($f=@file_get_contents($_SERVER['DOCUMENT_ROOT']."/{$freegamenewvar['maindb']}.txt")) {
            $query.=" AND find_in_set(t1.id_unique,\"$f\")";
        }
    }
    if ($exclusion) {
    	$query.=" AND find_in_set(t1.id_unique,'$exclusion')=0";
    }

	if ($orderby) $query.=" ORDER BY $orderby"; else $query.=" ORDER BY t1.freegame_name";
	if ($limit) $query.=" LIMIT $limit";
	return $query;	
}
function otherstoaddinhead() {
	global $adminrightafterbody; global $adminbeforebodycloses; global $sitedata; global $is_univ_sitemap; global $master_short;
    global $site_protocol; global $site;
	$s="";
	if ($master_short!="bcel") {
		if ($site['page_target']) {
			$s.="\n<link rel=\"alternate\" hreflang=\"{$sitedata['lan']}-{$site['page_target']}\" href=\"$site_protocol{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}\" />";
		} else {
			if ($sitedata['targeted_countries']) $lc= $sitedata['lan']."-".$sitedata['targeted_countries'];
			else $lc= $sitedata['lan'];
			$s.="\n<link rel=\"alternate\" hreflang=\"$lc\" href=\"$site_protocol{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}\" />";
		}
	}
    if ($is_univ_sitemap) {
    	$s.=<<<A
<style>
.sm_hierarchy {text-align:left;}
#univ_sitemap .sm_hierarchy {margin-bottom:0;}
#univ_sitemap .sm_hierarchy li {margin-bottom:0;}
#univ_sitemap>.sm_hierarchy>li>a {text-transform:uppercase; font-size:15px; font-weight:700;}
#univ_sitemap>.sm_hierarchy>li>ul>li>a {font-size:15px; font-weight:700;}
#univ_sitemap>.sm_hierarchy>li>ul>li>ul>li>a {font-size:14px;}
#univ_sitemap>.sm_hierarchy>li>ul>li>ul>li>ul>li>a {font-size:13px; font-style:italic;}
#univ_sitemap>.sm_hierarchy>li>ul>li>ul>li>ul>li>ul>li>a {font-size:12px;}
#univ_sitemap>.sm_hierarchy>li>ul>li>ul>li>ul>li>ul>li>ul>li {font-size:12px; font-style:italic;}
</style>        
A;
    }
    
// google analytics
	if ($sitedata['analytics_code']) {
    	$s.=<<<A

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id={$sitedata['analytics_code']}"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '{$sitedata['analytics_code']}');
</script>
        
A;
    }    
    
//push crew
	$query="select * from manage_pushcrew where url='{$_SERVER['HTTP_HOST']}'";
    $result=mnq($query);
    if ($result) {
        if ($r=mfs($result)) {
            if (preg_match("@^\s*<script.+</script>\s*$@is",$r['pushcrewcode'])) {
                $s.=$r['pushcrewcode'];
            }
            
             if (preg_match("@^\s*(<meta name\=\"google-site-verification\" content\=\".+\" />)\s*$@is",$r['google_site_verification'])) {
                $s.=$r['google_site_verification'];
            }
            
            if ($r['googletagmanagercode1']) $s.=$r['googletagmanagercode1'];
            if ($r['googletagmanagercode2']) $adminrightafterbody=$r['googletagmanagercode2'];
        }
    }
    return $s;
}
function freegame_banner_info($software_id) {
	global $sitelan; global $ipcountry; global $master_short; global $mastershort;
    if ($mastershort) $master_short=$mastershort;
	$a=array();
	$query="select * from live_banner_for_freegames where software_id='$software_id' and lan='$sitelan'";
	$result=mnq($query);
    if (!$result) return array();
	if ($r=mfs($result)) {
		if (preg_match_all("@^(.+)\:(.+)\r*$@mU",$r['casino_id'],$matches)) {
			$cs=array();
			foreach ($matches[1] as $mkey=>$mo) {
				$cs[$mo]=$matches[2][$mkey];
			}
			$a['casino_id']=$cs[$ipcountry] OR $a['casino_id']=$cs['default'];
			if ($a['casino_id']) {
				$query="select * from live_banner_for_freegames_text where casino_id='{$a['casino_id']}' and lan='$sitelan'";
				$result=mnq($query);
				if ($r=mfs($result)) {
					if (preg_match_all("@^(.+)\:(.+)\r*$@mU",$r['banner_text'],$matches)) {
						$tt=array();
						foreach ($matches[1] as $mkey=>$mo) {
							$tt[$mo]=$matches[2][$mkey];
						}
						$a['text']=$tt[$master_short] OR $a['text']=$tt['default'];
					}
				}
			}
		}
	}
	return $a;
}
function admin_mystripslashes(&$v,$key) {
	$v = stripslashes($v);	
}
function admin_mytrim(&$s,$key) {
	$s=trim($s);	
}

function getquotesfromarray($a) {
	$s=array();
	foreach ($a as $key=>$o) {
		if (substr($key,0,5)=="quote" and preg_match("@^quote(\d+)@",$key,$matches))
			$s[]="{$matches[1]}:{{".$o."}}";
	}
	return implode("\r\n",$s);
}
function getimagesfromarray($a,$images) {
	global $draftid;
	$old=array();
	preg_match_all("@^(\d+)\:([^/\r\n]+)@m",$images,$matches);
	foreach ($matches[1] as $kk=>$mm) {
		$old[$mm]=$matches[2][$kk];
	} 
	$s=array();
	foreach ($a as $key=>$o) {
		if (substr($key,0,9)=="imgselect" and $key!="imgselect0" and $key!="imgselect999" and preg_match("@^imgselect(\d+)@",$key,$matches)) {
			if ($o=="ori") $i=$old[$matches[1]]; elseif ($o=="") $i="none"; else $i="~~temp~~{$draftid}~$o";
			$s[]="{$matches[1]}:$i/{$a['imgfloat'.$matches[1]]}/{$a['imgwidth'.$matches[1]]}/{$a['imgheight'.$matches[1]]}/{$a['imgtext'.$matches[1]]}{}{$a['imgalt'.$matches[1]]}{}{$a['imglk'.$matches[1]]}";
		}
	}
	return implode("\r\n",$s);
}
function am_parent_url_of($url) {
	global $site; global $maintable; global $sitelan; global $subdomain;
    if (am_table_has_column($maintable,"lan")) $lanstring=" and lan='$sitelan'";
    if (am_table_has_column($maintable,"subdomain")) $subdomainstring=" and subdomain='$subdomain'";
    $query="select * from $maintable where page_url='$url' and theorder>0$lanstring$subdomainstring";
    $result=mnq($query);
    if ($r=mfs($result)) {
		return $r['parent_link'];
	}
}
function am_table_has_column($maintable,$col) {
	$query="show columns from $maintable";
    $result=mnq($query);
    while ($r=mfs($result)) {
    	if ($r['Field']==$col) return true;
    }
}
function am_type_of_url($url) {
	global $site; global $maintable; global $sitelan; global $subdomain;
    if (am_table_has_column($maintable,"lan")) $lanstring=" and lan='$sitelan'";
    if (am_table_has_column($maintable,"subdomain")) $subdomainstring=" and subdomain='$subdomain'";
    $query="select * from $maintable where page_url='$url' and theorder>0$lanstring$subdomainstring";
    $result=mnq($query);
    if ($r=mfs($result)) {
        if ($r['type']) return $r['type'];
        if ($r['type_admin']) return $r['type_admin'];
    }
}
function is_offspring_of_type($type,$url) {
	while ($url=am_parent_url_of($url)) {
    	if ($url=="/" or !$url) return false;
    	if (am_type_of_url($url)==$type) return true;
    }
}
function site_structure() {
	global $maintable;global $sitelan; global $subdomain;
    $site_structure=array();
    if (am_table_has_column($maintable,"lan")) $lanstring=" and lan='$sitelan'";
    if (am_table_has_column($maintable,"subdomain")) $subdomainstring=" and subdomain='$subdomain'";
    $query="select parent_link, page_url from $maintable where NULLIF(parent_link,'') IS NOT NULL and theorder>0$lanstring$subdomainstring order by theorder";
    $result=mnq($query);
    while ($r=mfs($result)) {
        $site_structure[$r['parent_link']][]=$r['page_url'];
    }
	return $site_structure;
}
function link2parents($link) { // like link1,link2...
	global $maintable; global $site_structure; global $sitelan; global $subdomain;
    if (!$site_structure) $site_structure=site_structure();
    
	global $link2parents_array;
    $link2parents_array=array();
    
    if (am_table_has_column($maintable,"lan")) $lanstring=" and lan='$sitelan'";
    if (am_table_has_column($maintable,"subdomain")) $subdomainstring=" and subdomain='$subdomain'";
    $query="select * from $maintable where (find_in_set(page_url,'$link')) and theorder>0$lanstring$subdomainstring";
    $result2=mnq($query);
    while ($r2=mfs($result2)) {
    	link2parents_process_url($r2['page_url']);
    }    
    return $link2parents_array;

}
function link2parents_process_url($url) {
	global $site_structure; global $link2parents_array;
    if (is_array($site_structure[$url])) {
    	$link2parents_array[]=$url;
    	foreach ($site_structure[$url] as $b) {
        	link2parents_process_url($b);
        }
    }
}
function type2parents($type) { // like type1,type2...
	global $maintable; global $site_structure; global $sitelan; global $subdomain;
    if (!$site_structure) $site_structure=site_structure();
    
	global $type2parents_array;
    $type2parents_array=array();
    
    if (am_table_has_column($maintable,"lan")) $lanstring=" and lan='$sitelan'";
    if (am_table_has_column($maintable,"subdomain")) $subdomainstring=" and subdomain='$subdomain'";
    
    $query="select * from $maintable where (find_in_set(type,'$type') or find_in_set(type_admin,'$type')) and theorder>0$lanstring$subdomainstring";
    $result2=mnq($query);
    while ($r2=mfs($result2)) {
    	type2parents_process_url($r2['page_url']);
    }    
    return $type2parents_array;
    
}
function type2parents_process_url($url) {
	global $site_structure; global $type2parents_array;
    if (is_array($site_structure[$url])) {
    	$type2parents_array[]=$url;
    	foreach ($site_structure[$url] as $b) {
        	type2parents_process_url($b);
        }
    }
    
}
function my_getimagesize($url) {
	if (preg_match("@^https?\://@i",$url)) {
        $headers = array(
        "Range: bytes=0-10485760" // Max 10MB
        );
    
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($curl);
        curl_close($curl);
    
        $im = imagecreatefromstring($data);
        
        $width = imagesx($im);
        $height = imagesy($im);
    
        return array($width,$height);
    } else return @getimagesize($url);
}
function my_exif_imagetype($url) {
    preg_match("@\.(gif|jpe?g|png)$@i",$url,$matches);
    $ty=strtolower($matches[1]);
    switch ($matches[1]) {
        case "gif":
            return 1;
            break;
        case "jpeg":
        case "jpg":
            return 2;
            break;
        case "png":
            return 3;
    }
}
function get_headers_curl($url)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,            $url);
    curl_setopt($ch, CURLOPT_HEADER,         true);
    curl_setopt($ch, CURLOPT_NOBODY,         true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT,        15);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

    $r = curl_exec($ch);
    return $r;
} 
function quote_in_article_general($matches) {
	global $site;
	$n=$matches[1];
	preg_match("@^$n\:(l|r|)(\d*)\{\{(.*)\}\}@m",$site['quotes'],$mm);
	if ($mm[3]) {
		return "<div class='excerpt1'>{$mm[3]}</div>";	
	}
}
function change_site_type_general() {
	global $site; global $maintable;
    if ($site['type']) return $site['type'];
    if ($site['type_admin']) return $site['type_admin'];

}
function do_migration_301($olddomain,$newdomain) {
	$olddomainquote=preg_quote($olddomain);
    $new_http_host=preg_replace("@(?<![^\.])$olddomainquote$@i","$newdomain",$_SERVER['HTTP_HOST']);
    if ($new_http_host!=$_SERVER['HTTP_HOST']) {
		$protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
    	header("Location: $protocol$new_http_host{$_SERVER['REQUEST_URI']}",TRUE,301);
    }
}
function visited_by_us() {
	$u=preg_quote("http://www.bonus-casino-en-ligne.info/manage/");
    if (preg_match("@$u@i",$_SERVER['HTTP_REFERER'])) return "from manage backend";
    if ($_GET['forceupdate']) return "forceupdate";
}

//mysqli_functions
function mnq($query) {
	global $mysqlilink;
    if ($mysqlilink) return mysqli_query($mysqlilink,$query);
    else return mysql_query($query);
}
function mfs($result) {
	if (!$result) return;
	global $mysqlilink;
	if ($mysqlilink) return $result->fetch_assoc();
    else return mysql_fetch_assoc($result);
}
function mfa($result) {
	if (!$result) return;
	global $mysqlilink;
	if ($mysqlilink) return $result->fetch_array();
    else return mysql_fetch_array($result);
}
function mnr($result) {
	if (!$result) return;
	global $mysqlilink;
	if ($mysqlilink) return $result->num_rows;
    else return mysql_num_rows($result);
}
function mres($s) {
	global $mysqlilink;
	if ($mysqlilink) return mysqli_real_escape_string($mysqlilink,$s);
    else return mysql_real_escape_string($s);
}
function mer() {
	global $mysqlilink;
	if ($mysqlilink) return mysqli_error($mysqlilink);
    else return mysql_error();
}
function merno() {
	global $mysqlilink;
	if ($mysqlilink) return mysqli_errno($mysqlilink);
    else return mysql_errno();
}
function mff($result) {
	if (!$result) return;
	global $mysqlilink;
	if ($mysqlilink) return mysqli_fetch_field($result);
    else return mysql_fetch_field($result);
}
function mar() {
	global $mysqlilink;
	if ($mysqlilink) return mysqli_affected_rows($mysqlilink);
    else return mysql_affected_rows();
}
function mii() {
	global $mysqlilink;
	if ($mysqlilink) return mysqli_insert_id($mysqlilink);
    else return mysql_insert_id();
}
function mds($result,$n) {
	if (!$result) return;
	global $mysqlilink;
	if ($mysqlilink) return mysqli_data_seek($result,$n);
    else return mysql_data_seek($result,$n);
}
function file_get_contents_https($url) {
	if (preg_match("@^https?\://@i",$url)) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;	
	} else return file_get_contents($url);
}
function sitemapxml() {
    global $maintable; global $sitedata;
    
	if (!$maintable) $maintable=$sitedata['db_tables'];

    if (!$sitedata) die("Not Available.");
	$site_table_criteria=site_table_criteria();
    $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
    header( 'Content-Type: application/xml' );
    echo '<?xml version="1.0" encoding="utf-8"?>' . "\n";
    echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    $query="select * from $maintable where theorder>0$site_table_criteria order by if(page_url='/',0,1), if(nullif(parent_link,'') is null,'zzzzz',parent_link), theorder";
    $result=mnq($query);
    while ($r=mfa($result)) {
        $mod=date("c", strtotime($r['last_insert_update_timestamp']." UTC"));
        $url=$r['page_url'];
        
        if ($maintable=="bcel_site" and $url=="/") $freq="daily";
        elseif ($url=="/") $freq="monthly";
        else $freq="yearly"; 
        
        if ($url=="/") $priority="1.0"; else $priority="0.7";
        
        $url=specialconvforsitemapxml($url);
        echo <<<A
<url>
<loc>$protocol{$sitedata['url']}$url</loc>
<lastmod>$mod</lastmod>
<changefreq>$freq</changefreq>
<priority>$priority</priority>
</url>
A;
    }
    echo '</urlset>';
    
}
function specialconvforsitemapxml($s) {
	$s=preg_replace("@&@","&amp;",$s);
	$s=strtr($s,array("'"=>"&apos;","\""=>"&quot;",">"=>"&gt;","<"=>"&lt;"));
	return $s;
}
function get_lan_locale($site) {
	global $sitelan; global $sitedata;
    if (!$sitelan or !$site['page_target']) {
        if (!$sitelan) $sitelan=$sitedata['lan'];
    }
	if ($site['page_target']) return $sitelan."-".$site['page_target'];
    else {
    	if ($sitedata['targeted_countries']) return $sitelan."-".$sitedata['targeted_countries'];
        else return $sitelan;
    }
}
function aweberform($awebercode,$strname,$stremail,$strconfirm,$confirmclass="",$thankyoupage="thankyou.html") {
    global $sitelan;
    $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
	$s=<<<A
<form id="newsletterForm" method="post" action="//www.aweber.com/scripts/addlead.pl">
<input type="hidden" name="listname" value="$awebercode" />
<input type="hidden" name="redirect" value="$protocol{$_SERVER['HTTP_HOST']}/$thankyoupage" />
<input type="hidden" name="meta_adtracking" value="custom form" />
<input type="hidden" name="meta_message" value="1" /> 
<input type="hidden" name="meta_required" value="name,email" /> 
<input type="hidden" name="meta_forward_vars" value="1" /> 
<input class="newsletterTextInputname txt nlf-field" onblur="if(this.value==''){this.value='$strname'}" onfocus="$('#newsletterinfo').text(''); if(this.value=='$strname'){this.value='';}" type="text" name="name" value="$strname" />
<input class="newsletterTextInputemail txt nlf-field" onblur="if(this.value==''){this.value='$stremail'}" onfocus="$('#newsletterinfo').text(''); if(this.value=='$stremail'){this.value='';}" type="text" name="email" value="$stremail" />
<input id="newslettersubmit" class='$confirmclass' type="submit" name="submit" value="$strconfirm" /> 
</form>
A;
	return $s;
}
function get_site_name() {
	global $sitedata;
    switch ($sitedata['header_text']) {
        case "none":
            $sitename="";
            break;
        case "":
            $sitename=preg_replace("@^.+\.@U","",$_SERVER['HTTP_HOST']);
            break;
        default:	
            $sitename=$sitedata['header_text'];
    }
	return $sitename;
}
function sitedata() {
	global $localmode; global $master_short; global $children_suffix; global $sites_locales;
    
    if ($localmode or $master_short=="bcel") {
        $query="select *, t1.short as short from manage_websites as t1 left join manage_websites_design as t2 on t1.short=t2.short 
        where t1.short='{$_SERVER['HTTP_HOST']}' or t1.url='{$_SERVER['HTTP_HOST']}'";
        $result=mnq($query);
        $sitedata=mfs($result);
        
        $query="select * from manage_websites where protocol='https' and theorder>0";
        $result=mnq($query);
        while ($r=mfs($result)) {
            $s[]=$r['url'];
        }
        $sitedata['httpssites']= implode(",",$s);
    } else {
        $sitedata=array();

        $s1=@file_get_contents($_SERVER['DOCUMENT_ROOT']."/{$_SERVER['HTTP_HOST']}.rdt");
        if ($s1) {
        	$odata=unserialize($s1);
            $sitedata=$odata;
        }
        $children_suffix=$sitedata['children_suffix'];

        if ($sitedata['targeted_countries']) $sites_locales[""]=$sitedata['lan']."-".$sitedata['targeted_countries']; 
        else $sites_locales[""]=$sitedata['lan'];
        
        if ($sitedata['children_suffix']) {
        	$csu=explode(",",$sitedata['children_suffix']);
            foreach ($csu as $cc) {
                $s2=@file_get_contents($_SERVER['DOCUMENT_ROOT']."/{$_SERVER['HTTP_HOST']}".strtr($cc,array("/"=>"_")).".rdt");
            	if ($s2) $tempdt=unserialize($s2); else $tempdt=array();
                if ($tempdt['targeted_countries']) $sites_locales[$cc]=$tempdt['lan']."-".$tempdt['targeted_countries']; 
                else $sites_locales[$cc]=$tempdt['lan'];
            	if (preg_match("@^{$cc}[/\?\#]@i",$_SERVER['REQUEST_URI']) and $tempdt) {
					$sitedata=$tempdt;
                    if (!$sitedata['id']) { // if no design data, use s1 design data
						$sitedata['header_text']=$odata['header_text'];
					}
                    $_SERVER['REQUEST_URI']=preg_replace("@^$cc@","",$_SERVER['REQUEST_URI']);
                    $_SERVER['REDIRECT_URL']=preg_replace("@^$cc@","",$_SERVER['REDIRECT_URL']);
				}                
            }
        }
    }

	$sitedata['root']=preg_replace("@^[^/]+@","",$sitedata['url'])."/";
    $sitedata['suffix']=substr($sitedata['root'],0,-1);
	return $sitedata;
}
function univ_social_account() {
	global $sitedata;
    if (!$sitedata) return;
    
	$query="select * from manage_social where site_id='{$sitedata['short']}'";
    $result=mnq($query);
    if (!$result) return;
    if ($r=mfa($result)) {
    	$a['fb']=$r['facebook'];
        $a['tw']=$r['twitter'];
        $a['gp']=$r['googleplus'];

        if (!$a['gp']) {
        	preg_match("@(.+)@", $r['googleplus_authors'], $matches);
            $e=explode(" ",$matches[1]);
            $a['gp']=$e[0];
        }
        return $a;
    }
}
function vlinktext() {
	global $sitedata; global $sitetypegeneral;

	$b['fr']['sport']="visiter le bookmaker";
	$b['fr']['poker']="visiter le poker room";
	$b['fr']['casino']="visiter le casino";

	$a['fr']="visiter";
	$a['de']="Spielen";
	$a['en']="visit";
	$a['es']="Visitar";
	$a['fi']="Tutustu";
	$a['sv']="Besök";
	$a['no']="Besøk";
	$a['it']="Visita";

	if ($sitedata) {
    	$lan=$sitedata['lan'];
        if ($b[$lan][$sitetypegeneral]) return $b[$lan][$sitetypegeneral];
        else return $a[$lan];
    } else {
    	global $sitelan;
        $lan=$sitelan;
        return $a[$lan];
    }
}
function review_id_name() {
	global $sitetypegeneral;
    switch ($sitetypegeneral) {
    	case "poker":
        	return "poker_id";
    	case "sport":
        	return "bm_id";
    	default:
        	return "casino_id";
    }
}

function sitetypegeneral() {
	global $sitedata;

    if (!in_array($sitedata['sitetype'],array("poker","sport"))) $tp="casino";
    else $tp=$sitedata['sitetype'];
    return $tp;
}
function gotovisit($id) {
	global $sitelan; global $sitedata; global $ipcountry;

    $ssite=$sitedata['short'];
    if (!$ssite) return;

	$hasfound=false;
    $ex=0;
    $vv="IS NULL";
    $special="";
    switch ($sitedata['sitetype']) {
    case "sport":
    case "poker":
		if ($sitedata['sitetype']=="sport") {
            $idd OR $idd="bm_id";
            $lkn OR $lkn="ps_links";
        } else {
            $idd OR $idd="poker_id";
            $lkn OR $lkn="bpel_links";
        }

		if (!$ex) {
			$query="select link from $lkn where $idd='$id' and link_id $vv and lan='$sitelan' and isexclusive = 1 and (only_for IS NULL or find_in_set('$ssite', only_for)>0) and (ex_as_normal='all' or find_in_set('$ssite', ex_as_normal)>0) ORDER BY only_for DESC LIMIT 1";
			$result=mnq($query);
			if (mnr($result)) $hasfound=true;
		}
		if (!$hasfound) {
			$query="select link from $lkn where $idd='$id' and link_id $vv and lan='$sitelan' and isexclusive = $ex and (only_for IS NULL or find_in_set('$ssite', only_for)>0) ORDER BY only_for DESC LIMIT 1";
			$result=mnq($query);
			if (mnr($result)) $hasfound=true;
		}
		if (!$hasfound) {
			$query="select link from $lkn where $idd='$id' and link_id IS NULL and lan='$sitelan' and isexclusive = 0 and (only_for IS NULL or find_in_set('$ssite', only_for)>0) ORDER BY only_for DESC LIMIT 1";
			$result=mnq($query);
			if (mnr($result)) $hasfound=true;
		}
		if (!$hasfound) {
			$query="select link from $lkn where $idd='$id' and link_id IS NULL and lan='en' and isexclusive = 0 and (only_for IS NULL or find_in_set('$ssite', only_for)>0) ORDER BY only_for DESC LIMIT 1";
			$result=mnq($query);
			if (mnr($result)) $hasfound=true;
		}
		
		if ($hasfound) {
			$r=mfa($result);
			$add=$r['link'];
		} else {$add="/";}
		
		$add=trim($add);
		if (array_key_exists("test",$_GET)) die($add);
		if ($add) {
			$add = "location: $add";
			header("HTTP/1.1 302 Moved Temporarily");
			header("$add");
            exit;
		}	    
    
        break;
    default:
    	if (!$ipcountry) $ipcountry=strtolower($sitedata['targeted_countries']);
        
        if ($special)
            $vv=" (link_id IS NULL or link_id = '$special') ";
        else
            $vv=" link_id IS NULL ";
    
        if ($ismobilephone) $mobilefilter=" AND IFNULL(pc_only,0)=0";
            else $mobilefilter=" AND IFNULL(mobile_only,0)=0";
        
        if ($ex) $exfilter=""; else $exfilter=" and (isexclusive =0 OR (isexclusive =1 AND ex_as_normal='1'))";
    
        $foripsfilter = " and  (NULLIF(forips,'') IS NULL or if (instr(forips, 'not for'), FIND_IN_SET('{$GLOBALS['ipcountry']}',replace(forips,'not for ',''))=0, FIND_IN_SET('{$GLOBALS['ipcountry']}',forips)))";
    
        $query="select * from bcelnew_links where casino_id='$id'$mobilefilter$exfilter$foripsfilter and $vv and lan='$sitelan' and (only_for IS NULL or find_in_set('$ssite', only_for)>0)
                ORDER BY mobile_only DESC, link_id DESC, isexclusive DESC, only_for DESC, forips DESC, id DESC";
		$result=mnq($query);
        if ($result) {
            if ($r=mfs($result)) {
                $add=$r['link'];
            }
        }
        if (!$add and $sitelan!="en") { // if didn't find link in the given language, use english link instead
            $query="select * from bcelnew_links where casino_id='$id'$mobilefilter$exfilter$foripsfilter and $vv and lan='en' and (only_for IS NULL or find_in_set('$ssite', only_for)>0)
                ORDER BY mobile_only DESC, link_id DESC, isexclusive DESC, only_for DESC, forips DESC, id DESC";
            $result=mnq($query);
            if ($result) {
               	if ($r=mfa($result)) {
                	$add=$r['link'];
                }
            }
        }
        $add=trim($add);
		if (array_key_exists("test",$_GET)) die($add);
        if ($add) {
            $add = "location: $add";
            header("HTTP/1.1 302 Moved Temporarily");
            header("$add");
            exit;
        }
    }
        

}
function no_data_for_order($useorder) {
	if ($useorder=="order_general") return false;
	global $sitedata;
	$query="select id from manage_aff_orders where site_id='{$sitedata['short']}' and $useorder>0";
    $result=mnq($query);
    if (!mfs($result)) return true;
}
function hp_beginningtext_and_toplist() {
	global $sitedata; global $site; global $sitelan; global $maintable; global $sitetypegeneral;
    global $ordername;
    if (!$ordername or no_data_for_order($ordername)) $useorder="order_general"; else $useorder=$ordername;
    if ($site['page_url']!="/") return;

	if (!$site_table_criteria) $site_table_criteria=site_table_criteria();
    
	switch ($sitetypegeneral) {
    	case "sport": $logofolder="bmlogo"; $idname="bm_id"; $ext="png"; $usetable="bookmakers"; break;
    	case "poker": $logofolder="pokerlogo"; $idname="poker_id"; $ext="png"; $usetable="pokerrooms"; break;
    	default: $idname="casino_id"; $logofolder="casinologo"; $ext="gif"; $usetable="casinos";
    }

    $reviewtext=array("en"=>"Review","fr"=>"Revue","de"=>"Rezension","es"=>"Análisis","sv"=>"Recension","fi"=>"Arvostelu","no"=>"Anmedelse","it"=>"Recensione","ro"=>"Recenzie","pt"=>"Crítica");
    $visittext=array("en"=>"PLAY NOW","fr"=>"JOUER MAINTENANT","de"=>"Spielen","es"=>"Visitar","sv"=>"Besök","fi"=>"Tutustu","no"=>"Besøk","it"=>"Visita","ro"=>"Visit","pt"=>"Visit");
    $toptext["bookmakers"]=array("en"=>"TOP X BOOKMAKERS","fr"=>"TOP X BOOKMAKERS","de"=>"TOP X BOOKMAKERS","es"=>"TOP X BOOKMAKERS","sv"=>"TOP X BOOKMAKERS","fi"=>"TOP X BOOKMAKERS","no"=>"TOP X BOOKMAKERS","it"=>"TOP X BOOKMAKERS","ro"=>"TOP X BOOKMAKERS","pt"=>"TOP X BOOKMAKERS");
    $toptext["pokerrooms"]=array("en"=>"TOP X POKER ROOMS","fr"=>"TOP X POKER ROOMS","de"=>"TOP X POKER ROOMS","es"=>"TOP X POKER ROOMS","sv"=>"TOP X POKER ROOMS","fi"=>"TOP X POKER ROOMS","no"=>"TOP X POKER ROOMS","it"=>"TOP X POKER ROOMS","ro"=>"TOP X POKER ROOMS","pt"=>"TOP X POKER ROOMS");
    $toptext["casinos"]=array("en"=>"TOP X CASINOS","fr"=>"TOP X CASINOS","de"=>"TOP X CASINOS","es"=>"TOP X CASINOS","sv"=>"TOP X CASINOS","fi"=>"TOP X CASINOS","no"=>"TOP X CASINOS","it"=>"TOP X CASINOS","ro"=>"TOP X CASINOS","pt"=>"TOP X CASINOS");
    $reviewtext=$reviewtext[$sitelan] OR $reviewtext="Review"; 
    $visittext=$visittext[$sitelan] OR $visittext="Visit";
    
    $s=$site['site_main_2'];
    
    if(array_key_exists($idname,$site)) $f_idname=$idname;
    elseif (array_key_exists("site_id",$site)) $f_idname="site_id";
    
    if ($f_idname) {
        $query="select t1.*, t2.page_url from manage_aff_orders as t1 left join $maintable as t2 
        on t1.the_id = t2.$f_idname and (t2.type='review' OR IFNULL(t2.type,'')='') and t2.theorder>0$site_table_criteria    
        where t1.site_id='{$sitedata['short']}' and t1.$useorder>0 order by t1.$useorder";
    } else
        $query="select t1.* from manage_aff_orders as t1 
        where t1.site_id='{$sitedata['short']}' and t1.$useorder>0 order by t1.$useorder";

    $result=mnq($query);
    if ($result) {
    	$i=0;
        while ($r=mfs($result)) {
            $img="img/$logofolder/{$r['the_id']}.$ext";
            $stars=stars_in_top_list($i++);
            if (file_exists_after_converting($img,150,60,"http://www.bonus-casino-en-ligne.info/img/$logofolder/")) {
                $img="<img class='nrz' src='/$img' />";
            } else {
                $img="";
            }
            if ($r['page_url']) $rl="<a href='{$r['page_url']}'>$reviewtext</a>"; else $rl="";
            $thename=id2name($usetable,$r['the_id']);
            $t.="
<tr><td class='no'>$i.</td><td class='img'><a{$GLOBALS['nofollow']} href='/go/{$r['the_id']}.html' target='_blank'>$img</a></td><td class='nm'>$thename</td><td class='stars'>$stars</td>
<td class='r'>$rl</td><td class='v'><a{$GLOBALS['nofollow']} href='/go/{$r['the_id']}.html' target='_blank'>$visittext</a></td></tr>";
        }
    }
    $topt=$toptext[$usetable][$sitelan];
    $topt=strtr($topt,array(" X "=>" $i "));
    if ($t) $s.="<table cellpadding='0' cellspacing='0' id='top_list1'><caption><span>$topt</span></caption>$t</table>";
    return $s;
}
function id2name($usetable,$id) {
	global $id2name;
    if (!$id2name) {
    	$id2name=array();
        $u=array("casinos"=>"casino_id","bookmakers"=>"bm_id","pokerrooms"=>"poker_id");
        $query="select * from $usetable";
        $result=mnq($query);
        if ($result) {
            while ($r=mfs($result)) {
                $id2name[$r[$u[$usetable]]]=$r['name'];
            }
        }
    }
	return $id2name[$id];

}
function stars_in_top_list($n) {
	$a=array(0=>50,1=>45,2=>45,3=>40,4=>40);
    $s=<<<A
<div class='starsbg'><div class='starsfg stars{$a[$n]}'></div></div>    
A;
	return $s;
}
function site_table_criteria() {
	global $sitedata;
    if ($sitedata['table_criteria']) 
    return " AND ".strtr($sitedata['table_criteria'],array("*lan*"=>$sitedata['lan'],"*subdomain*"=>preg_replace("@\..+$@","",$sitedata['url'])));
    
    if (preg_match("@/.+@",$sitedata['url'],$matches)) 
    	return " AND subsite='{$matches[0]}'";
    elseif ($sitedata['children_suffix']) return " AND subsite IS NULL";
}
function get_sitelan_from_sitedata() {
	global $sitedata;
    return $sitedata['lan'];
}
function get_maintable_from_sitedata() {
	global $sitedata;
    return $sitedata['db_tables'];
}
function topmenugeneral() {
	global $sitedata; global $headerwithtopmenu; global $site;
    if(!$sitedata['topmenu_enabled']) return;
	if($sitedata['topmenu_content']) {
    	preg_match_all("@<a href\=[\'|\"](.+)[\'|\"].*>(.+)</a>@Ui",$sitedata['topmenu_content'],$matches);
        foreach ($matches[0] as $ind=> $m) {
        	if ($matches[1][$ind]==$site['page_url']) 
            	$l="<span class='cur'>{$matches[2][$ind]}</span>"; 
            else 
            	$l="<a$class href='{$matches[1][$ind]}'>{$matches[2][$ind]}</a>";
        	$headerwithtopmenu=" headerwithtopmenu";
        	if ($i++>0) $s.="<li class='sep'><span></span></li>";
        	$s.="<li>$l</li>";
        }
    
    	return "<div id='topmenugeneral'><ul>$s</ul></div>";;
	}
}
function adjust_https($s) {
	global $sitedata; global $allhttpssites;
    $u=$sitedata['httpssites'];
    if (!$u) return $s;
    
    $allhttpssites=explode(",",$u);
    
    $s=preg_replace_callback("@http\://([^/\'\"\s]+)\b@i","adjust_https_callback",$s);
    return $s;
}
function adjust_https_callback($matches) {
	global $allhttpssites;
    if (in_array($matches[1],$allhttpssites)) return "https://{$matches[1]}"; else return $matches[0];
}
function is_https($s) {
	global $sitedata;
    $u=$sitedata['httpssites'];
    $u=explode(",",$u);
    
	preg_match("@^https?\://([^/]+)@i",$s,$matches);
    if(in_array($matches[1],$u)) return true;
}
function linkstoothers() {
	global $children_suffix; global $sites_locales;
	if (!$children_suffix) return;

    $a=<<<A
fr-AD	Andorre
fr-BE	Belgique
fr-BF	Burkina Faso
fr-BI	Burundi
fr-BJ	Bénin
fr-CA	Canada
fr-CD	République Démocratique du Congo
fr-CF	République Centrafricaine
fr-CG	Congo
fr-CH	Suisse
fr-CI	Côte D'ivoire
fr-CM	Cameroun
fr-DJ	Djibouti
fr-FR	France
fr-GA	Gabon
fr-GB	Royaume-Uni
fr-GF	Guinée Française
fr-GN	Guinée
fr-GP	Guadeloupe
fr-HT	Haïti
fr-IT	Italie
fr-KM	Comores
fr-LB	Liban
fr-LU	Luxembourg
fr-MC	Monaco
fr-MG	Madagascar
fr-ML	Mali
fr-MQ	Martinique
fr-NC	Nouvelle-Calédonie
fr-NE	Niger
fr-PF	Polynésie française
fr-PM	Saint-Pierre et Miquelon
fr-RE	Réunion
fr-RW	Rwanda
fr-SC	Les Seychelles
fr-TD	Tchad
fr-TG	Togo
fr-VA	Vatican – Le Saint-Siège
fr-VU	Vanuatu
fr-WF	Wallis-et-Futuna
fr-YT	Mayotte
en-AG	Antigua And Barbuda
en-AI	Anguilla
en-AS	American Samoa
en-AU	Australia
en-BB	Barbados
en-BE	Belgium
en-BM	Bermuda
en-BN	Brunei Darussalam
en-BS	Bahamas
en-BW	Botswana
en-BZ	Belize
en-CA	Canada
en-CK	Cook Islands
en-CM	Cameroon
en-DM	Dominica
en-ER	Eritrea
en-ET	Ethiopia
en-FJ	Fiji
en-FK	Falkland Islands (Malvinas)
en-FM	Micronesia
en-GB	United Kingdom
en-GD	Grenada
en-GH	Ghana
en-GI	Gibraltar
en-GM	Gambia
en-GU	Guam
en-GY	Guyana
en-HK	Hong Kong
en-IE	Ireland
en-IL	Israel
en-IN	India
en-IO	British Indian Ocean Territory
en-JM	Jamaica
en-KE	Kenya
en-KI	Kiribati
en-KN	Saint Kitts And Nevis
en-KY	Cayman Islands
en-LC	Saint Lucia
en-LR	Liberia
en-LS	Lesotho
en-MH	Marshall Islands
en-MP	Northern Mariana Islands
en-MS	Montserrat
en-MT	Malta
en-MU	Mauritius
en-MW	Malawi
en-NA	Namibia
en-NF	Norfolk Island
en-NG	Nigeria
en-NR	Nauru
en-NU	Niue
en-NZ	New Zealand
en-PG	Papua New Guinea
en-PH	Philippines
en-PK	Pakistan
en-PN	Pitcairn
en-PR	Puerto Rico
en-PW	Palau
en-RW	Rwanda
en-SB	Solomon Islands
en-SC	Seychelles
en-SG	Singapore
en-SH	Saint Helena
en-SL	Sierra Leone
en-SO	Somalia
en-SZ	Swaziland
en-TC	Turks And Caicos Islands
en-TK	Tokelau
en-TO	Tonga
en-TT	Trinidad And Tobago
en-UG	Uganda
en-UM	United States Minor Outlying Islands
en-US	United States
en-VC	Saint Vincent And The Grenadines
en-VG	Virgin Islands, British
en-VI	Virgin Islands, U.S.
en-VU	Vanuatu
en-WS	Samoa
en-ZA	South Africa
en-ZM	Zambia
en-ZW	Zimbabwe
es-AR	Argentina
es-BO	Bolivia
es-CL	Chile
es-CO	Colombia
es-CR	Costa Rica
es-CU	Cuba
es-DO	República Dominicana
es-EC	Ecuador
es-ES	España
es-GQ	Guinea Ecuatorial
es-GT	Guatemala
es-HN	Honduras
es-MX	México
es-NI	Nicaragua
es-PA	Panamá
es-PE	Perú
es-PR	Puerto Rico
es-PY	Paraguay
es-SV	El Salvador
es-US	Estados Unidos
es-UY	Uruguay
es-VE	Venezuela
de-AT	Österreich
de-BE	Belgien
de-CH	Schweiz
de-DE	Deutschland
de-DK	Dänemark
de-FR	Frankreich
de-HU	Ungarn
de-IT	Italien
de-LI	Liechtenstein
de-LU	Luxemburg
de-PL	Polen
sv-AX	Åland
sv-FI	Finland
sv-SE	Sverige
fi-FI	Suomi
fi-SE	Ruotsi
no-NO	Norge
fr	France
de	Deutschland
en	United Kingdom
fi	Suomi
sv	Sverige
es	España
no	Norge
A;

	preg_match_all("@^(.+)\t(.*?)\r*$@m",$a,$matches);
    
    foreach ($matches[1] as $i=>$j) {
    	$m[$j]=$matches[2][$i];
    }
    
    foreach ($sites_locales as $k=>$o) {
    	$s.="<a class='ftlinks' href='$k/'>{$m[$o]}</a>";
    }
    return $s;
}
function should_block($ip) {
	global $ipblocklist; global $citiestoblock; global $sitedata; global $ipcountry;
    
    if ($sitedata['lan']!="fr") return false;

    $ipcountry=strtolower(geoip($ip));
    if ($ipcountry!="fr") return false;

    foreach ($ipblocklist as &$ipb) {
        $ipb=preg_quote($ipb);
    }
    $ipbs=implode("|",$ipblocklist);
    if (preg_match("@^($ipbs)\..+@",$ip)) return true;

    $town=strtolower(geoip_city($ip));
    if (in_array($town,$citiestoblock)) return true; else return false;
}
function aff_order_case($siteid,$order_addhere) {
	global $toplistversioncontrol;
	$query="select * from manage_aff_orders where site_id='$siteid'$toplistversioncontrol and order$order_addhere>0 order by order$order_addhere";
    $result=mnq($query);
    while ($r=mfs($result)) {
    	$n++;
    	$s.="WHEN '{$r['the_id']}' THEN $n ";
    }
    if ($s) return "CASE casino_id $s ELSE 99 END, ";
}
function valid_casinos_bookmakers_pokerrooms() {
	global $sitetypegeneral; global $sitelan; global $ipcountry; global $site;

    $a=array(); $b="";
	switch ($sitetypegeneral) {
    	case "sport": $logofolder="bmlogo"; $idname="bm_id"; $ext="png"; $usetable="bookmakers"; break;
    	case "poker": $logofolder="pokerlogo"; $idname="poker_id"; $ext="png"; $usetable="pokerrooms"; break;
    	default: $idname="casino_id"; $logofolder="casinologo"; $ext="gif"; $usetable="casinos";
    }
    if(array_key_exists($idname,$site)) $f_idname=$idname;
    elseif (array_key_exists("site_id",$site)) $f_idname="site_id";
    else return array($a,$b,$f_idname);
    
    $query="select $idname from $usetable where valid=1";
    if (in_array($sitelan,array("en","fr","de","es","fi","sv","no"))) $query.=" and lan_$sitelan = 1";
    if (in_array($ipcountry,array("us","es","be","gb","ca","it","nz","fr"))) {
    	$allowfld=array("us"=>"us","es"=>"spain","be"=>"belgium","gb"=>"uk","ca"=>"canada","it"=>"italy","nz"=>"nz","fr"=>"france");
    	$query.=" and {$allowfld[$ipcountry]}_allowed =1";
    }
    if ($result=mnq($query)) {
    	while ($r=mfs($result)) {
        	$a[]=$r[$idname];
        }
    }
    
    if ($a) {
        $b=implode(",",$a);
        $b=" AND (NULLIF($f_idname,'') is null OR find_in_set($f_idname,'$b'))";
    }
    
	return array($a,$b,$f_idname);
}
function getJour_univ($day) {
	global $sitelan;
    switch ($sitelan) {
    case "fr":
        $jour["Monday"] = "Lundi";
        $jour["Tuesday"] = "Mardi";
        $jour["Wednesday"] = "Mercredi";
        $jour["Thursday"] = "Jeudi";
        $jour["Friday"] = "Vendredi";
        $jour["Saturday"] = "Samedi";
        $jour["Sunday"] = "Dimanche";
        return $jour[$day];
    case "es":
        $jouer["Monday"] = "lunes";
        $jouer["Tuesday"] = "martes";
        $jouer["Wednesday"] = "miércoles";
        $jouer["Thursday"] = "jueves";
        $jouer["Friday"] = "viernes";
        $jouer["Saturday"] = "sábado";
        $jouer["Sunday"] = "domingo";
        return $jour[$day];
    case "de":
        $jouer["Monday"] = "Montag";
        $jouer["Tuesday"] = "Dienstag";
        $jouer["Wednesday"] = "Mittwoch";
        $jouer["Thursday"] = "Donnerstag";
        $jouer["Friday"] = "Freitag";
        $jouer["Saturday"] = "Samstag";
        $jouer["Sunday"] = "Sonntag";
        return $jour[$day];
    case "fi":
        $jour["Monday"] = "maanantai";
        $jour["Tuesday"] = "tiistai";
        $jour["Wednesday"] = "keskiviikko";
        $jour["Thursday"] = "torstai";
        $jour["Friday"] = "perjantai";
        $jour["Saturday"] = "lauantai";
        $jour["Sunday"] = "sunnuntai";
        return $jour[$day];
    case "sv":
        $jour["Monday"] = "Måndag";
        $jour["Tuesday"] = "Tisdag";
        $jour["Wednesday"] = "Onsdag";
        $jour["Thursday"] = "Torsdag";
        $jour["Friday"] = "Fredag";
        $jour["Saturday"] = "Lördag";
        $jour["Sunday"] = "Söndag";
        return $jour[$day];
    case "no":
        $jour["Monday"] = "mandag";
        $jour["Tuesday"] = "tirsdag";
        $jour["Wednesday"] = "onsdag";
        $jour["Thursday"] = "torsdag";
        $jour["Friday"] = "fredag";
        $jour["Saturday"] = "lørdag";
        $jour["Sunday"] = "søndag";
        return $jour[$day];
	}    
}
function getMois_univ($month){
	global $sitelan;
    switch ($sitelan) {
    case "fr":
        $mois["January"] = "janvier";
        $mois["February"] = "février";
        $mois["March"] = "mars";
        $mois["April"] = "avril";
        $mois["May"] = "mai";
        $mois["June"] = "juin";
        $mois["July"] = "juillet";
        $mois["August"] = "ao&ucirc;t";
        $mois["September"] = "septembre";
        $mois["October"] = "octobre";
        $mois["November"] = "novembre";
        $mois["December"] = "décembre";
        return $mois[$month];
    case "es":
        $mois["January"] = "enero";
        $mois["February"] = "febrero";
        $mois["March"] = "marzo";
        $mois["April"] = "abril";
        $mois["May"] = "mayo";
        $mois["June"] = "junio";
        $mois["July"] = "julio";
        $mois["August"] = "agosto";
        $mois["September"] = "septiembre";
        $mois["October"] = "octubre";
        $mois["November"] = "noviembre";
        $mois["December"] = "diciembre";
        return $mois[$month];
    case "de":
        $mois["January"] = "Januar";
        $mois["February"] = "Februar";
        $mois["March"] = "März";
        $mois["April"] = "April";
        $mois["May"] = "Mai";
        $mois["June"] = "Juni";
        $mois["July"] = "Juli";
        $mois["August"] = "August";
        $mois["September"] = "September";
        $mois["October"] = "Oktober";
        $mois["November"] = "November";
        $mois["December"] = "Dezember";
        return $mois[$month];
    case "fi":
        $mois["January"] = "tammikuu";
        $mois["February"] = "helmikuu";
        $mois["March"] = "maaliskuu";
        $mois["April"] = "huhtikuu";
        $mois["May"] = "toukokuu";
        $mois["June"] = "kesäkuu";
        $mois["July"] = "heinäkuu";
        $mois["August"] = "elokuu";
        $mois["September"] = "syyskuu";
        $mois["October"] = "lokakuu";
        $mois["November"] = "marraskuu";
        $mois["December"] = "joulukuu";
        return $mois[$month];
    case "sv":
        $mois["January"] = "januari";
        $mois["February"] = "februari";
        $mois["March"] = "mars";
        $mois["April"] = "april";
        $mois["May"] = "maj";
        $mois["June"] = "juni";
        $mois["July"] = "juli";
        $mois["August"] = "augusti";
        $mois["September"] = "september";
        $mois["October"] = "oktober";
        $mois["November"] = "november";
        $mois["December"] = "december";
        return $mois[$month];
    case "no":
        $mois["January"] = "januar";
        $mois["February"] = "februar";
        $mois["March"] = "mars";
        $mois["April"] = "april";
        $mois["May"] = "mai";
        $mois["June"] = "juni";
        $mois["July"] = "juli";
        $mois["August"] = "august";
        $mois["September"] = "september";
        $mois["October"] = "oktober";
        $mois["November"] = "november";
        $mois["December"] = "desember";
        return $mois[$month];
    default:
    	return $month;
    }
}
function af_listpagesofsite($fil="") {
	global $maintable; global $site_table_criteria; global $master_short;
    
	$tablesused[0]=$maintable;
	if ($master_short=="tcc") {
    	$tablesused[1]="casinos_plus";
    }
    
    
	$a=array();
	foreach ($tablesused as $tb) {
		$mainqueries[$tb]="select * from $tb where (type IS NULL or type !='news') and theorder>0$site_table_criteria order by theorder";
	}

	foreach ($mainqueries as $tablename=>$mainquery) {
		hierarchy($mainquery, $tablename);
	}
	
	return af_process_hierarchy($fil);
	
}
function af_process_hierarchy($fil) {
	global $url; global $pp;
	foreach ($pp as $page_url=>$o) {
		 global $f;
		 $f=array();
		 traceback($page_url);

		$f=array_reverse($f);

		$c=count($f);

		switch($c) {
			case 1:
				$rel[$f[0]][""]=1;
				break;
			case 2:
				$rel[$f[0]][$f[1]][""]=1;
				break;
			case 3:
				$rel[$f[0]][$f[1]][$f[2]][""]=1;
				break;
			case 4:
				$rel[$f[0]][$f[1]][$f[2]][$f[3]][""]=1;
				break;
			case 5:
				$rel[$f[0]][$f[1]][$f[2]][$f[3]][$f[4]][""]=1;
				break;
			case 6:
				$rel[$f[0]][$f[1]][$f[2]][$f[3]][$f[4]][$f[5]][""]=1;
				break;
			case 7:
				$rel[$f[0]][$f[1]][$f[2]][$f[3]][$f[4]][$f[5]][$f[6]][""]=1;
				break;
			case 8:
				$rel[$f[0]][$f[1]][$f[2]][$f[3]][$f[4]][$f[5]][$f[6]][$f[7]][""]=1;
				break;
			case 9:
				$rel[$f[0]][$f[1]][$f[2]][$f[3]][$f[4]][$f[5]][$f[6]][$f[7]][$f[8]][""]=1;
				break;
			case 10:
				$rel[$f[0]][$f[1]][$f[2]][$f[3]][$f[4]][$f[5]][$f[6]][$f[7]][$f[8]][$f[9]][""]=1;
				break;
			case 11:
				$rel[$f[0]][$f[1]][$f[2]][$f[3]][$f[4]][$f[5]][$f[6]][$f[7]][$f[8]][$f[9]][$f[10]][""]=1;
				break;
		}
	}
	if ($pp) {
        adjustorder($rel);
        adjustorder_root($rel);
        if ($fil) {
        	$rel=addfil($rel,$fil);
        }
        $s= "<div id='univ_sitemap'>".af_showhierarchy($rel, "")."</div>";
	}
    return $s;
}
function addfil($rel,$fil) {
    global $ffff;
    $ffff="";
    relfilter($rel,$fil);
    if ($ffff) $rel=array($fil=>$ffff);
    return $rel;
}
function relfilter($r,$fil) {
	global $ffff;
    if (!$ffff and is_array($r)) {
        foreach ($r as $key=>$v) {
            if ($key=="$fil") {
                $ffff=$v;
                return;
            }
            else relfilter($v,$fil);
        }
    }
}
function hierarchy($query, $tablename) {
	global $pp; global $p2c;
	$result=mnq($query);
    if ($result) {
        while ($r=mfa($result)) {
        	if ($r['type']=="review" or $tablename=="casinos_plus") {
            	$tid=$r['casino_id'] OR $tid=$r['bm_id'] OR $tid=$r['poker_id'] OR $tid=$r['site_id'];
                if (!casino_bm_pr_is_valid($tid)) continue;
            }
            $maintitle=$r['maintitle'] OR $maintitle=$r['main_title'] OR $maintitle=$r['menutitle'] OR $maintitle=$r['game_name'] OR $maintitle=$r['team_name'] OR $maintitle=$r['menu_text'] OR $maintitle=$r['plus_name'] OR $maintitle=$r['bank_name'] OR $maintitle=$r['page_url'];
            
            $txt=$r['maintext'] . $r['site_main_2'] . $r['site_main_3'];
            
            if ($r['parent_link'] and !preg_match("@\{.+\}@", $txt)) $noparentlink=1; else $noparentlink=0;
            
            if (trim($r['bottom_text'])) $nobottomtext=0; else $nobottomtext=1;
    
            if (in_array($tablename,array("bcel_site","game"))) $r['parent_link']=get_parentlink_bcel($r,$tablename);
            
            $ppk=trim($r['parent_link']);
            if ($ppk==$r['page_url']) $ppk="";
            
            $metatitle=$r['metatitle'] OR $metatitle=$r['meta_title'];
            $metadescription=$r['metadescription'] OR $metadescription=$r['meta_description'];
            $pp[$r['page_url']]=array($ppk,$maintitle,$r['theorder']+1-$r['id']/10000000,$noparentlink, $nobottomtext,$metatitle,$metadescription,$r['menutitle'],$r['id']); 
            $p2c[$r['parent_link']][]=$r['page_url'];
        }
	} else {
    	$pp=array();
        $p2c=array();
    }
	
}
function adjustorder(&$rel) {
	global $pp;
	$a=array();
	if (is_array($rel)) {
		foreach ($rel as $k=>$u) {
			$a[]=$pp[$k][2];
		}
		array_multisort($a,$rel);
		foreach ($rel as &$u) {
			//if (is_array($u)) 
			adjustorder($u);
		}
	}
}
function af_showhierarchy($rel, $no) {
	global $pp; global $sitedata; global $site_protocol;
	if (is_array($rel)) {
		$v=0;
		foreach ($rel as $k=>$u) {
			if ($k!=="") {
			$v++;
			$lll="";
			if ($no and $pp[$k][1]) $lll="<a target='_blank' href='$site_protocol{$sitedata['url']}$k'>{$pp[$k][1]}</a>";

			if ($lll) $s.="<ul class='sm_hierarchy'><li>$lll".af_showhierarchy($u, "$no$v.")."</li></ul>";
            else $s.=af_showhierarchy($u, "$no$v.");
		}
		}
		return $s;
	}
}
function traceback($page_url) {
	global $pp; global $f;
		$f[]=$page_url;
		if ($pp[$page_url][0]) {
			traceback($pp[$page_url][0]);
		}
	
}
function adjustorder_root(&$rel) {
	ksort($rel);
		
}
function hasbrother($k) {
	global $p2c; global $pp;
	
	$p=$pp[$k][0]; 
	
	if ($p and is_array($p2c[$p]) and count($p2c[$p])>1) return true;
		
}
function metatitle_field($maintable) {
	$query="select meta_title from $maintable limit 0";
    $result=mnq($query);
    if ($result) return "meta_title"; else return "metatitle";
}
function casino_bm_pr_is_valid($id) {
	global $sitetypegeneral; global $allvalidforthetype;
    if (!$allvalidforthetype) {
    	$allvalidforthetype=array();
        switch ($sitetypegeneral) {
            case "poker":
                $query="select poker_id as d from pokerrooms where valid>0";
                break;
            case "sport":
                $query="select bm_id as d from bookmakers where valid>0";
                break;
            default:
                $query="select casino_id as d from casinos where valid>0";
        }
        $result=mnq($query);
        while ($r=mfs($result)) {
        	$allvalidforthetype[]=$r['d'];
        }
    }
    if (in_array($id,$allvalidforthetype)) return true;
}
function sitehasmorethan1page() {
	global $maintable; global $site_table_criteria;
	$query="select count(*) from $maintable where page_url !='/sitemap.html' and theorder>0$site_table_criteria";
    $result=mnq($query);
    if ($result) {
    	$r=mfs($result);
        if ($r['count(*)']>1) return true;
    }
}
function sites_using_the_banking($bid,$sitetype="casino") {
    $a=array();
	$tbs=array("sport"=>"bookmakers","casino"=>"casinos","poker"=>"pokerrooms");
	$idd=array("sport"=>"bm_id","casino"=>"casino_id","poker"=>"poker_id");
	$query="select {$idd[$sitetype]} from {$tbs[$sitetype]} where valid=1 and banking_id REGEXP \"([',']|^){$bid}['(']\"";
    if ($result=mnq($query)) {
    	while ($r=mfs($result)) {
        	$a[]=$r[$idd[$sitetype]];
        }
    }
    return $a;
}
function sites_using_the_software($sid,$sitetype="casino") {
    $a=array();
	$tbs=array("sport"=>"bookmakers","casino"=>"casinos","poker"=>"pokerrooms");
	$idd=array("sport"=>"bm_id","casino"=>"casino_id","poker"=>"poker_id");
	$query="select {$idd[$sitetype]} from {$tbs[$sitetype]} where valid=1 and find_in_set('$sid',software_id)";
    if ($result=mnq($query)) {
    	while ($r=mfs($result)) {
        	$a[]=$r[$idd[$sitetype]];
        }
    }
    return $a;
}
function univ_contact() {
	global $sitelan;
	$GLOBALS['adminbeforebodycloses']=<<<A
<script>
$("#univcontactsubmit").click(function(){
    var hasError = false;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var lan = $("#contact_lan").val();
    var t_emptyfield = $("#contact_1").val();
    var t_emailinvalid = $("#contact_2").val();
    var t_success = $("#contact_4").val();
    var t_validationerror = $("#contact_3").val();
    
    $("#spanname").text("");
    $("#spanemail").text("");
    $("#spanmessage").text("");
    $("#contactresponse").text("");
    
    var emailVal = $.trim($("#youremail").val());
    if(emailVal == '') {
        $("#spanemail").text(t_emptyfield);
        hasError = true;
    } else if(!emailReg.test(emailVal)) {
        $("#spanemail").text(t_emailinvalid);
        hasError = true;
    } 
    
    var msgVal = $.trim($("#yourmessage").val());
    if(msgVal == '') {
        $("#spanmessage").text(t_emptyfield);
        hasError = true;
    }
    
    var nameVal = $.trim($("#yourname").val());
    if(nameVal == '') {
        $("#spanname").text(t_emptyfield);
        hasError = true;
    }
    if(hasError == false) {
        $.post("/univemail.html",
        { name: nameVal, email: emailVal, message: msgVal, contactlan: lan },
            function(data){
                $("#contactresponse").text(t_success);
                $("#yourname").val("");
                $("#youremail").val("");
                $("#yourmessage").val("");
            }
        );  
    } else {
        $("#contactresponse").html("<span style='color:red;'>"+t_validationerror+"</span>");
    }
    
    return false;
});
</script>    
A;

    switch ($sitelan) {
    	case "en":
        	$t=array("Your name","Your Email","Type your message","Send",
            "Please fill the required field.",
            "Email address seems invalid.",
            "Validation errors occurred. Please confirm the fields and submit it again.",
            "Your message was sent successfully. Thanks.",);
        	break;
    	case "fr":
        	$t=array("Votre nom","Votre Email","Tapez votre message","Envoyer",
            "Veuillez remplir les champs requis.",
            "L'adresse Email semble invalide.",
            "Erreur de validation. Veuillez vérifier les champs et soumettre à nouveau.",
            "Votre message a bien été envoyé. Merci.",);
        	break;
    	case "de":
        	$t=array("Ihr Name","Ihre E-Mail Adresse","Geben Sie hier Ihre Nachricht ein","Senden",
            "Bitte füllen Sie die erfordelrichen Felder aus.",
            "Die E-Mail Adresse scheint ungültig zu sein.",
            "Bestätigungsfehler. Bitte überprüfen Sie die Angaben und reichen Sie diese erneut ein.",
            "Die Nachricht wurde erfolgreich verschickt. Danke.",);
        	break;
    	case "es":
        	$t=array("Su nombre","Su correo electrónico","Escriba su mensaje","Enviar",
            "Por favor complete los campos obligatorios.",
            "Dirección de correo electrónico no es válido.",
            "Error de validación. Por favor revise los campos y enviar otra vez.",
            "Su mensaje fue enviado con éxito. Gracias.",);
        	break;
    	case "fi":
        	$t=array("Nimesi","Sähköpostiosoitteesi","Kirjoita viestisi","Lähetä",
            "Ole hyvä ja täytä pyydetyt tiedot.",
            "Email osoite näyttää väärältä.",
            "Ilmeni vahvistusvirhe. Ole hyvä ja varmista antamasi tiedot ja syötä ne uudestaan.",
            "Viestisi lähetettiin onnistuneesti. Kiitos.",);
        	break;
    	case "sv":
        	$t=array("Ditt namn","Din e-postadress","Din fråga/ditt ärende","Skicka",
            "Var så god och fyll I fältet.",
            "E-post verkar vara fel ifylld.",
            "Fel med validering. Var så god, kola fälten och skicka igen.",
            "Ditt meddelande har skickats. Tack.",);
        	break;
    	case "no":
        	$t=array("Ditt navn","Din epost","Skriv inn din melding","Sende",
            "Vær så snill och fyll inn i alle feltene.",
            "E-mail adressen er ikke gyldig.",
            "Feil under validering. Vennligst sjekk feltene og send på nytt.",
            "Beskjed ble sendt. Takk.",);
        	break;
    }
    
	$s=<<<A
<form style='width:100%;margin:0 0 50px;' action="#" method="post" class="contact-form">
<div>
	{$t[0]} : <span id='spanname' style='color:red;'></span>
	<div><input style='border:1px #ccc solid;padding:5px;width:95%;margin:5px 0 10px;' id='yourname' type="text" value="" /></div>
</div>
<div>
	{$t[1]} : <span id='spanemail' style='color:red;'></span>
	<div><input style='border:1px #ccc solid;padding:5px;width:95%;margin:5px 0 10px;' id='youremail' type="text" value="" /></div>
</div>
<div>
	{$t[2]} : <span id='spanmessage' style='color:red;'></span>
	<div><textarea style='border:1px #ccc solid;padding:5px;width:95%;margin:5px 0 10px;height:200px;' id='yourmessage'></textarea></div>
</div>
<div>
	<input id='univcontactsubmit' style='padding:5px 20px;margin:20px 0 0;text-transform:uppercase;' type='button' value='{$t[3]}' />
</div>
<div id="contactresponse" style='color:green;margin-top:10px;'></div>
<input id='contact_1' type="hidden" value="{$t[4]}" />
<input id='contact_2' type="hidden" value="{$t[5]}" />
<input id='contact_3' type="hidden" value="{$t[6]}" />
<input id='contact_4' type="hidden" value="{$t[7]}" />
<input id='contact_lan' type='hidden' value="$sitelan" />
</form>
A;
	return $s;
}
function univ_stripslashes_deep($value)
{
    $value = is_array($value) ?
                array_map('univ_stripslashes_deep', $value) :
                stripslashes($value);

    return $value;
}
function univ_ipcountry() {
	global $sitedata; global $ipcountry; global $iphaschecked; global $remoteip; global $countryadmin; global $univ_live_admin;
	global $ordername; global $univ_checkip;
    
    $sitelan=$sitedata['lan'];
    
	if ($_GET['admin']=='0' or $_GET['admin']=='1')  {SetCookie("countryadmin", $_GET['admin']);}
	if ($_GET['lan']) {SetCookie("lan", $_GET['lan']); $_COOKIE['lan']=$_GET['lan'];}
	if ($_GET['admin']=='1' or $_COOKIE["countryadmin"]=="1") {$countryadmin=true;}
	if ($_GET['admin']=='0') {$countryadmin=false;}

	$ipswecheck=array('fr'=>'fr,ch,be,ca','en'=>'gb,us,ca,za,au','de'=>'de,at,ch','es'=>'es,us,ar,pe,cl,co,mx,ve,pr','fi'=>'fi',
'sv'=>'se','no'=>'no');

	if (!$ipswecheck[$sitelan]) return;
	
    $univ_countriestocheck[$sitelan]=explode(",",$ipswecheck[$sitelan]);
    
    if (!$countryadmin) {
        if (!$iphaschecked) $ipcountry=file_get_contents_https("https://www.casinoonlinefrancais.info/ipcountry.php?remoteip=$remoteip");
        $ipcountry=strtolower($ipcountry);
    } else $ipcountry=$_COOKIE['lan'];
    
    if (!in_array($ipcountry,$univ_countriestocheck[$sitelan])) {
    	$ipcountry="others";
        $ordername="order_general";
    } else {
        $ordername="order_{$ipcountry}_ip";
        $ctc=array("fr"=>"france","ca"=>"canada","be"=>"belgium","es"=>"spain","us"=>"us","gb"=>"uk","nz"=>"nz");
        if ($ctc[$ipcountry]) $univ_checkip=" AND {$ctc[$ipcountry]}_allowed=1";
		$univ_checkip.=" and (NULLIF(forips,'') IS NULL or if (instr(forips, 'not for'), 
        FIND_IN_SET('$ipcountry',replace(forips,'not for ',''))=0, 
        FIND_IN_SET('$ipcountry',forips)))";
    }

    if ($countryadmin) {
		$ctoname=array("fr"=>"France","ca"=>"Canada","be"=>"Belgium","ch"=>"Switzerland","us"=>"USA","es"=>"Spain",
		"gb"=>"United Kingdom","at"=>"Austria","de"=>"German","fi"=>"Finland","se"=>"Sweden","no"=>"Norway",
		"mx"=>"Mexico","ar"=>"Argentine","cl"=>"Chile","au"=>"Australia","za"=>"South Africa","co"=>"Columbia",
		"pe"=>"Peru","ve"=>"Venezuela","pr"=>"Puerto Rico","others"=>"General");
		$univ_countriestocheck[$sitelan][]="others";
    	foreach ($univ_countriestocheck[$sitelan] as $c) {
	        if ($ipcountry!=$c) 
	        	$u.= "<a style='font-weight:normal;margin-right:10px;' href='/?lan=$c'>{$ctoname[$c]}</a>";
            else
	        	$u.= "<span style='font-weight:700;color:green;margin-right:10px;'>{$ctoname[$c]}</span>";
        }
        $univ_live_admin= <<<A
<div id='trans1' style='position:absolute;right:0;top:0;background:#fff;margin:0;padding:5px 15px;z-index:999999;'>$u<a href='/?admin=0'><small>Exit</small></a></div>
A;
    }
}
function wagerdetailsnew2() {
	global $site; global $master_short;
	global $maxbonus; global $ff; global $maintypename; global $defaultcurarray;
	global $maxbonusplus;
    global $nolink; global $ismobilephone;
	global $r1; global $bonustypes; global $finalsubtype; global $defaultcur; global $dict; global $sitelan;

    if ($master_short=="bcel") $specialtracking=specialtracking();
    else $exclusivecri=" AND is_exclusive_bonus =0";
    
    $id=$site['casino_id'];
	
    $query = "select * from bonuses_new where
    	 onlyfornotfor='onlyfor $sitelan'$exclusivecri
         AND NULLIF(bonus_type, '') IS NOT NULL AND casino_id=\"$id\" AND (star>0 or star is null) 
         ORDER BY is_exclusive_bonus, nth, max_bonus DESC";
         
	$g=array();
    $result=mnq($query);
    while ($r=mfa($result)) {
        $nth=(int)$r['nth'];
        if (!$nth) $nth=1;
    	$bonusname=bonusname($r);
        $bonusmax=bonusmax($r); 
        $bonuspercentnew=calc_bonus_match($bonusmax,$r['deposit4maxbonus']);
        if ($bonuspercentnew) $bonuspercentnew.="%"; else $bonuspercentnew="-";
        $bonusmax=currencychange($bonusmax,"",0,1);
		if ($r['max_bonus_plus']) {
			if ($bonusmax!="&ndash;") $bonusmax.="<span>+ {$r['max_bonus_plus']}</span>";
			else $bonusmax=$r['max_bonus_plus'];
		}
        $bonusname=wgtrans($bonusname);
        if ($r['is_exclusive_bonus']) {
        	$ex=1;
        	$goorex="ex";
            $bonusname.="<br /><b style='margin:5px 0 0 0;' class='exc'>{$dict['Exclusif']}</b>";
            $gbo=$dict["Obtenir le bonus exclusif"];
            if ($specialtracking[$r['casino_id']][$r['bonus_type']]) 
                $plusornot="?bonus={$r['bonus_type']}";
            else $plusornot="";
			$changepadding="";
       } else {
            $ex=0;
            $goorex="go";
            $gbo=$dict['Obtenir le bonus(bt)'];
            if (strpos($gbo, "<br />")===false) $changepadding=" style='padding-top:11px;'"; else $changepadding="";
            $plusornot="";
        }
		if ($nolink) $colspan=4; else $colspan=5;
        if (!$nolink) {
        	switch ($master_short) {
            	case "bcel":
                	$vlk="<a$changepadding class='btn1b' href='{$GLOBALS['protocol']}://{$_SERVER['HTTP_HOST']}/casino/{$site['casino_id']}/$goorex.html$plusornot'>$gbo</a>";
                    $vlkmobile="<a style='padding-top:11px;' class='btn1b' href='{$GLOBALS['protocol']}://{$_SERVER['HTTP_HOST']}/casino/{$site['casino_id']}/$goorex.html$plusornot'>$gbo2</a>";
                	break;
                case "clf":
                	$gbo=strtr($gbo,array("<br />"=>" "));
                	$vlk="<a class='button btnb' href='/go/{$site['casino_id']}.html' target='_blank'>$gbo</a>";
                    $vlkmobile="<a class='button btnb' href='/go/{$site['casino_id']}.html' target='_blank'>$gbo</a>";
                	break;
            
            }
        
        
			$tdlast="<td class='gg'>$vlk</td>";
			$colspan2=$colspan-1;
			$gbo2=strtr($gbo,array("<br />"=>" "));
			if ($ismobilephone) $tdlastfm="<td colspan='$colspan2'>$vlkmobile</td>";
		}
        if ($sitelan!="de") $cashcd="<td class='cc'>$cashable</td>";
        $star=(int)$r['star'];
		
		$stable=stable($r);
		if ($tdlastfm) $ma="<tr class='tr_fm'>$tdlastfm</tr>";
    	$g[$star][$r['bonus_type']][$nth][$ex][]=<<<A
<tr><td class='bn'>$bonusname</td>
<td class='pc'>$bonuspercentnew</td>
<td class='bm'>$bonusmax</td>
<td class='moreinfo'><div></div></td>
$tdlast
</tr>
<tr class='tr_moreinfo'><td colspan='$colspan'>$stable</td></tr>
$ma
A;
        
    }
    
	$g=wgsort($g);

	foreach ($g as &$x) {
		foreach ($x as &$xx) {
			foreach ($xx as &$xxx) {
				foreach ($xxx as &$xxxx) {
					$xxxx=array_unique($xxxx);
				}
			}
		}
	}

	$s="";
    foreach ($g as $gg) {    
        foreach ($gg as $btype=> $gi) {
            foreach ($gi as $gii) {
                foreach ($gii as $giii) {
                    foreach ($giii as $giiii) {
                        $s.=$giiii;
                    }
                }
            }
        }
    }


    if ($s) {
    	if ($hascondition) $ccc="<th>{$dict['wagering_requirements']}</th>";
        if ($hascode) $ddd="<th>{$dict['Bonus Code(bt)']}</th>";
        if (!$nolink) $lastfield="<th class='gg'>{$dict['Obtenir(bt)']}</th>";
        if ($sitelan!="de") $cashth="<th class='gg'>{$dict['Bonus Encaissable(bt)']}</th>";
        if ($master_short=="bcel") {
            global $deplusname;
            eval("\$bon = \"{$dict['Bonus details']}\";");
        	$caption="<caption class='tab-title'>$bon</caption>";
        }
    	$s=<<<A
<table id="wagertablenew">
$caption
<thead><tr><th>{$dict['Les bonus(bt)']}</th><th>{$dict['Taux(bt)']}</th><th>{$dict['Montant Maximum(bt)']}</th><th class='moreinfo'>{$dict['more info']}</th>$lastfield</tr></thead>
<tbody>$s</tbody>
</table>  
A;
		$s="<div id='wnt'>$s</div>";
}
    return $s;
}
function bonusname($r) {
	if ($r['bonus_type']=="Welcome") {
    	return addthnew($r['nth']);
    } else return $r['bonus_type'];
}
function addthnew($n) {
	global $sitelan;
	$n=(int)$n;
    if ($n==0) $n=1;
	switch ($sitelan) {
    	case "fr":
            $s= "Bonus " . $n; 
            if ($n==1) {$s.= '<sup>er</sup>';} else {$s.= '<sup>ème</sup>';} 
            $s.= " dépôt";   
            return $s;  
        case "de":
            return "$n. Einzahlungsbonus";
        case "fi":
            return "$n. Talletusbonus";
        case "no":
        	return "$n. Innskuddbonus";
        
		case "es":
			switch ($n) {
				case 1:
					return "Bono primer depósito";
				default:
					return "Bono {$n}º depósito";
			}
        case "sv":
			switch ($n) {
				case 1:
                case 2:
                	return "{$n}:a Insättningsbonus";
                default:
                	return "{$n}:e Insättningsbonus";
			}        
		default:
			if ($n % 10 ==1 AND $n %100 != 11) $v= $n."<sup>st</sup> Deposit Bonus";
			elseif ($n % 10 ==2 AND $n %100 != 12) $v= $n."<sup>nd</sup> Deposit Bonus";
			elseif ($n % 10 ==3 AND $n %100 != 13) $v= $n."<sup>rd</sup> Deposit Bonus";
			else $v= $n."<sup>th</sup> Deposit Bonus";
            if ($sitelan=="en") return $v; else return "($v)";
	}
}
function bonusmax($r) {
	global $dict; global $sitelan;
	if ($r['bonus_type']=="Freeplay")
		return freeplay_bonus_change($r['freeplay_bonus']);
    elseif ($r['bonus_type']=="Freespins")
    	return $r['max_bonus'] . " " . $dict['free spins'];
    else return currencychange($r['max_bonus'],"",1);
}
function wgtrans($s) {
	global $dict;
    if ($dict['(bonus)'.$s]) return $dict['(bonus)'.$s];
    else return $s;
}
function stable($r) {
	global $dict; global $gamesallowed;
    $wgbonuscode=wgbonuscode($r);
	$cashable=($r['notcashable'])?$dict["no"]:$dict["yes"];
	$cashable=ucfirst($cashable);
    $wgtitle=wgtitle($r);
	if ($gamesallowed) $gl=implode(", ",$gamesallowed); else $gl=$dict['newreview_not_available'];
	
	if ($wgtitle) {
		if (count($wgtitle)>1) {
			foreach ($wgtitle as $w=>$g) {
				$wg.="<div class='wg'><span>$w:</span>$g</div>";
			}
		} else {
			$wg="<span>".current($wgtitle)."</span>";
		}
	}
	
	if ($r['bonus_note']) $nt=$r['bonus_note'];
	
	$s.="<tr><td class='bv'><div class='icon'></div>{$dict['bonus available on']}:</td><td>$gl</td></tr>";
	if ($wgtitle) $s.="<tr><td class='cd'><div class='icon'></div>{$dict['wagering_requirements']}:</td><td class='cond'>$wg</td></tr>";
	$s.="<tr><td class='ec'><div class='icon'></div>{$dict['Bonus Encaissable(bt)']}:</td><td>$cashable</td></tr>";
	$s.="<tr><td class='bc'><div class='icon'></div>{$dict['Bonus Code(bt)']}:</td><td>$wgbonuscode</td></tr>";
	if ($nt) $s.="<tr><td class='nt'><div class='icon'></div>{$dict['Note']}:</td><td>$nt</td></tr>";
	return "<div><div class='shadowtop'></div><table class='stable'>$s</table><div class='shadowbottom'></div></div>";
}
function wgbonuscode($r) {
	if ($r['code']) return "<b>".$r['code']."</b>"; else return "-";
}
function wgtitle($r) {
	global $dict; global $gamesallowed;
	$u=array(); $gamesallowed=array();
	$dms=  array("(wager)wr_slots","(wager)wr_video_poker","(wager)wr_american_roulette","(wager)wr_other_roulette","(wager)wr_classic_blackjack","(wager)wr_other_blackjack","(wager)wr_baccarat","(wager)wr_bingo","(wager)wr_casino_war","(wager)wr_craps","(wager)wr_parlor_games","(wager)wr_poker_games","(wager)wr_sicbo");
	foreach ($dms as $d) {
    	$dd=substr($d,7);
        if ($r[$dd]!="") {
			if ($r[$dd]=="0") {
				$u[$dict[$d]]=$dict['No wagering requirements'];
			} else {
				$u[$dict[$d]]= strtr($r[$dd], array("b"=>" ".$dict['bonus'],"d"=>" ".$dict['deposit'], "&"=>" &"));
				$u[$dict[$d]]=strtolower(preg_replace("@(\d+)x@", "$1 x",$u[$dict[$d]]));
			}
			switch ($dd) {
				case "wr_american_roulette":	
				case "wr_other_roulette":
					$gamesallowed[]=$dict['Roulette'];
					break;
				case "wr_classic_blackjack":
				case "wr_other_blackjack":
					$gamesallowed[]=$dict['Blackjack'];
					break;
				default:
					$gamesallowed[]=$dict[$d];
			}
		}
    }	
	$gamesallowed=array_unique($gamesallowed);
    return $u;
}
function mywgsort($a) {
    $bonusno=array(
    "Nodeposit" => "01", 
    "Freeplay" => "02", 
    "Freespins" => "03", 
    "Welcome" => "04", 
    "Midroller" => "05", 
    "Highroller" => "06",
    "VIP"=> "07",
    "Monthly" => "08",
    "Weekly" => "09",
    "Weekend" => "10");

	$b=array();
	foreach ($a as $key=>$u) {
    	if ($bonusno[$key]) $b[]=$bonusno[$key]; else $b[]="99".$key;
    }
    array_multisort($b,$a);
    return $a;
}
function wgsort($g) {
	krsort($g);
	foreach ($g as &$gg) {
    	$gg=mywgsort($gg);
        foreach ($gg as &$gi) {
            ksort($gi);
            foreach ($gi as &$gii) {
                krsort($gii);
            }
        }
    }
    return $g;
}
function univ_indexfollow() {
	global $indexfollow; global $site;
    if ($indexfollow) return $indexfollow;
    else {
    	if ($site['nofollow']) $nf="nofollow"; else $nf="follow";
    	if ($site['noindex']) $ni="noindex"; else $ni="index";
        return "<meta name='robots' content='$ni, $nf' />\n";
    }
}
function univ_process_multipages($query="") {
	global $sitedata; global $site; global $site_protocol; global $resultlistnews; global $prevnext;
    global $page_count; global $allrows_count; global $indexfollow; global $articlesperpage; global $sitelan;
    global $thepage; global $maintable; global $rows_per_page;global $usequestionmark;
    global $currentpage;
    
	if (preg_match("@(\d+)/$@",$_SERVER['REQUEST_URI'],$matches)) {
    	$currentpage=$matches[1];
    } else {
        $currentpage=intval($_GET['page']) OR $currentpage=$thepage;
    }
    if (!$currentpage) $currentpage=1;
    if ($currentpage>1) $indexfollow="<meta name='robots' content='noindex, follow' />\n";
    switch ($sitedata['short']) {
    case "cfo":
    case "cs":
	    $rows_per_page = $articlesperpage;
        $offset = ($currentpage - 1) * $rows_per_page;
	    $query="select SQL_CALC_FOUND_ROWS * from $maintable where type='news' AND theorder>0 and lan='$sitelan' ORDER BY id DESC limit $offset, $rows_per_page";
    	break;
    case "clf":
        $usequestionmark=1;
        $rows_per_page = 21;
        $offset = ($currentpage - 1) * $rows_per_page;
        switch ($site['type_admin']) {
            case "newsindex":
                $query = "select SQL_CALC_FOUND_ROWS * from clf_site WHERE type='news' AND theorder>0 order by adding_date DESC LIMIT $offset, $rows_per_page; ";
                break;
            case "guidecasinoindex":
                $type2parents_string=implode(",",type2parents("guidecasinoindex"));
                $query="select SQL_CALC_FOUND_ROWS * from clf_site where theorder>0
AND find_in_set(parent_link,'$type2parents_string') order by id DESC LIMIT $offset, $rows_per_page";
                break;
            case "regles-strategies":
                $type2parents_string=implode(",",type2parents("game_index"));
                $query="select SQL_CALC_FOUND_ROWS * from clf_site where theorder>0
AND find_in_set(parent_link,'$type2parents_string') AND page_url NOT LIKE '/jeux/%' order by id DESC LIMIT $offset, $rows_per_page";
                break;
        }
        break;
    default:
        $offset = ($currentpage - 1) * $rows_per_page;
        $query.=" limit $offset, $rows_per_page";
    }
    $resultlistnews = mnq($query);
    $rows = mnr($resultlistnews);
    $sql="SELECT FOUND_ROWS() AS 'found_rows'; ";
    $result1 = mnq($sql);
    $myrow = mfs($result1);
    $allrows_count = $myrow['found_rows'];
    $page_count = ceil($allrows_count / $rows_per_page);

    if ($usequestionmark) {
        $w=preg_replace("@\?.*@","",$_SERVER['REQUEST_URI']);
        if ($currentpage<$page_count and $currentpage>0) {
            $prevnext="<link rel='next' href='$site_protocol{$_SERVER['HTTP_HOST']}$w?page=".($currentpage+1)."'>\n";
        }
        if ($currentpage>1 and $currentpage<=$page_count) {
            $pvn=$currentpage-1;
            if ($pvn==1) $pvns=""; else $pvns="?page=$pvn";
            $prevnext.="<link rel='prev' href='$site_protocol{$_SERVER['HTTP_HOST']}$w$pvns'>\n";
        }     
	} else {
        $w=preg_replace("@\d+/$@","",$_SERVER['REQUEST_URI']);
        if ($currentpage<$page_count and $currentpage>0) {
            $prevnext="<link rel='next' href='$site_protocol{$_SERVER['HTTP_HOST']}$w".($currentpage+1)."/'>\n";
        }
        if ($currentpage>1 and $currentpage<=$page_count) {
            $pvn=$currentpage-1;
            if ($pvn==1) $pvns=""; else $pvns="$pvn/";
            $prevnext.="<link rel='prev' href='$site_protocol{$_SERVER['HTTP_HOST']}$w$pvns'>\n";
        }     
    
    }
    if ($page_count>1) {
        $site['meta_title'].=" $currentpage/$page_count";
        $site['metatitle'].=" $currentpage/$page_count";
        $site['maintitle'].=" $currentpage/$page_count";
        if ($currentpage>1) {
            $site['maintext']="";
            $site['site_main_2']="";
            $site['bottom_text']="";
        }	
    }
}