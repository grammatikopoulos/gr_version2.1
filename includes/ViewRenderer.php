<?php
/*
put this define in the root folder
define('__ROOT__', dirname(dirname(__FILE__))); 

then put the templates files in root /templates folder
  
  usage :
	$data = $this->repository->get_data();
	echo ViewRenderer::render('widget.html', $data);
	
	sample widget.html content :
	
	<div class="row">
		<a href="<?php echo $data->url; ?>" rel="nofollow" target="_blank">
			<img src="<?php echo $data->image; ?>" ></img>
		</a>
	</div>
	
*/

class ViewRenderer {

    private static function get_template_location($filename) {
      return  __ROOT__ .'/templates/'.$filename;
    }

    public static function render($tpl, $data) {
      ob_start();
      include(ViewRenderer::get_template_location($tpl));
      $output = str_replace(array("\r", "\n"), '', ob_get_contents());
      ob_end_clean();
      return $output;
    }
  }
